import 'package:apsicon2019/detail/program_details.dart';
import 'package:apsicon2019/pages/subprogram.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Program extends StatefulWidget {
  @override
  _Program createState() => _Program();
}

class _Program extends State<Program> with TickerProviderStateMixin {
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(vsync: this, length: 5);
    
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Agenda'),
        centerTitle: true,
      ),
      body: TabBarView(
        controller: _controller,
        children: <Widget>[    
          SubProgram(day: 'programday1final'),
          SubProgram(day: 'programday2final'),
          SubProgram(day: 'programday3final'),
          SubProgram(day: 'programday4final'),
          SubProgram(day: 'programday5final'),
        ],
      ),
      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: new TabBar(
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: _controller,
          indicatorWeight: 4.0,
          indicator: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15.0),
                  bottomRight: Radius.circular(15.0))),
          tabs: <Tab>[
            new Tab(text: "11 Dec"),
            new Tab(text: "12 Dec"),
            new Tab(text: "13 Dec"),
            new Tab(text: "14 Dec"),
            new Tab(text: "15 Dec")
          ],
        ),
      ),
    );
  }

    Widget _buildProgramDay1(BuildContext contex, DocumentSnapshot document) {
    
    return Container(
      color: Theme.of(context).primaryColorLight,
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Stack(children: <Widget>[
              Card(
                color: Colors.white,
                margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 2.0),
                elevation: 0.0,
                child: Container(
                  padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                  height: 110.0,
                  child: ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${document['type']}",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w500),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "${document['topic']}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),

                        document['time']!=""?
                        Row(
                          children: <Widget>[
                            
                            Icon(
                              Icons.access_time,
                              color: Theme.of(context).primaryColor,
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                              child: Text(
                                document['Time'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontStyle: FontStyle.italic),
                              ),
                              alignment: Alignment(-1, 0),
                            ),
                            Icon(
                              Icons.access_time,
                              color: Theme.of(context).primaryColor,
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                              child: Text(
                                document['Time'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontStyle: FontStyle.italic),
                              ),
                              alignment: Alignment(-1, 0),
                            ),
                          ],
                        ):Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
            onTap: () {
              
              // if ((document['type'] == "Abstract Presentation") ||
              //     (document['type'] == "Moderation")) {
              //   Navigator.of(context).push(new MaterialPageRoute(
              //       builder: (context) => new ProgramDetails(
                          
              //             time: document['Time'],
              //             topic: document['Topic'],
              //             date: document['date'],
              //             type: document['type'],
              //             // abstracts: document['Abstract'],
              //             // chairperson: document['Chairperson'],
              //             // number: document['num'],
              //             // moderator: document['Moderator'],
              //             // panellist: document['Panellist'],
              //             // session: document['Session'],
              //           )));
              // }
            },
          ),
        ],
      ),
    );
  }

  // Widget _buildProgramDay1(BuildContext contex, DocumentSnapshot document) {
    
  //   return Container(
  //     color: Theme.of(context).primaryColorLight,
  //     child: Column(
  //       children: <Widget>[
  //         GestureDetector(
  //           child: Stack(children: <Widget>[
  //             Card(
  //               color: Colors.white,
  //               margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 2.0),
  //               elevation: 0.0,
  //               child: Container(
  //                 padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
  //                 height: 110.0,
  //                 child: ListTile(
  //                   title: Column(
  //                     crossAxisAlignment: CrossAxisAlignment.start,
  //                     children: <Widget>[
  //                       Text(
  //                         "${document['type']}",
  //                         style: TextStyle(
  //                             color: Theme.of(context).primaryColor,
  //                             fontWeight: FontWeight.w500),
  //                       ),
  //                       Row(
  //                         children: [
  //                           Expanded(
  //                             child: Text(
  //                               "${document['Topic']}",
  //                               style: TextStyle(
  //                                   color: Colors.black,
  //                                   fontWeight: FontWeight.w500),
  //                               maxLines: 2,
  //                             ),
  //                           ),
  //                         ],
  //                       ),
  //                       SizedBox(
  //                         height: 10,
  //                       ),

  //                       document['Time']!=""?
  //                       Row(
  //                         children: <Widget>[
                            
  //                           Icon(
  //                             Icons.access_time,
  //                             color: Theme.of(context).primaryColor,
  //                           ),
  //                           Container(
  //                             padding: const EdgeInsets.fromLTRB(5.0, 0, 0, 0),
  //                             child: Text(
  //                               document['Time'],
  //                               style: TextStyle(
  //                                   color: Colors.black,
  //                                   fontStyle: FontStyle.italic),
  //                             ),
  //                             alignment: Alignment(-1, 0),
  //                           ),
  //                         ],
  //                       ):Container(),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ]),
  //           onTap: () {
              
  //             if ((document['type'] == "Abstract Presentation") ||
  //                 (document['type'] == "Moderation")) {
  //               Navigator.of(context).push(new MaterialPageRoute(
  //                   builder: (context) => new ProgramDetails(
                          
  //                         time: document['Time'],
  //                         topic: document['Topic'],
  //                         date: document['date'],
  //                         type: document['type'],
  //                         // abstracts: document['Abstract'],
  //                         // chairperson: document['Chairperson'],
  //                         // number: document['num'],
  //                         // moderator: document['Moderator'],
  //                         // panellist: document['Panellist'],
  //                         // session: document['Session'],
  //                       )));
  //             }
  //           },
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
