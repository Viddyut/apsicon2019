import 'dart:ui';
import 'package:apsicon2019/common/separator.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Faculty extends StatefulWidget {
  @override
  _Faculty createState() => _Faculty();
}

class _Faculty extends State<Faculty> with SingleTickerProviderStateMixin {
  TabController controller;
  bool horizontal;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text('Speakers'),centerTitle: true,),
      
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
          image: DecorationImage(image:AssetImage('assets/lord.jpg'), fit: BoxFit.fill ) 
        ),
          
       padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 14.0),
            child: Stack(
              children: <Widget>[
                 Positioned(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 2.5, sigmaY: 2.5),
                                        child: Container(
                                          color: Colors.black.withOpacity(0),
                                        ),
                                      ),
                                    ),
                StreamBuilder(
                  stream: Firestore.instance.collection('international_faculty').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    return ListView.builder(
                     
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) =>
                          _international(context, snapshot.data.documents[index]),
                    );
                  },
                ),
              ],
            ),
          ),
              Container(
            decoration: BoxDecoration(
          image: DecorationImage(image:AssetImage('assets/lord.jpg'), fit: BoxFit.fill ) 
        ),
          
       padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
            child: Stack(
              children: <Widget>[
                 Positioned(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 2.5, sigmaY: 2.5),
                                        child: Container(
                                          color: Colors.black.withOpacity(0),
                                        ),
                                      ),
                                    ),
                StreamBuilder(
                  stream: Firestore.instance.collection('national_faculty').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    return ListView.builder(
                     
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) =>
                          _national(context, snapshot.data.documents[index]),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),

      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        
        child: new TabBar(
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: controller,
          indicatorPadding: const EdgeInsets.only(top: 0.0),         
          indicator: BoxDecoration(color: Colors.white),
          tabs: <Tab>[new Tab(text: "International Faculty"), new Tab(text: "National Faculty")],
        ),
      ),
    );
  }
}

Widget _national(BuildContext contex, DocumentSnapshot document) {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
    elevation: 2.0,
    child: ListTile(
      leading: CircleAvatar(
        child: Text(document['Name'][0]),
      ),
      title: Row(
        children : [
          Expanded(
            child: Text(document['Name']),
          ),
        ],
      ),
      // subtitle: Text(""),
      // onTap: () {},
    ),
  );
}

Widget _international(BuildContext contex, DocumentSnapshot document) {
  return Column(
    children: <Widget>[
      GestureDetector(
        child: Stack(children: <Widget>[
          Card(

            color: Theme.of(contex).primaryColor,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            margin: new EdgeInsets.fromLTRB(46.0, 0.0, 6.0, 15.0),
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.only(left: 40.0, top: 5, bottom: 10),
              height: 118.0,
              child: ListTile( 
                title: Row(
                  children: [
                    Expanded(
                      child: Text(document['Name'], style: TextStyle(color: Colors.white),),
                    ),
                  ],
                ),
                subtitle: Column(
                  children: <Widget>[
                    Container(alignment: Alignment(-1, 0), child: Separator()),
                    Container(alignment: Alignment(-1, 0), child: Text(document['City'], style: TextStyle(color: Colors.white),)),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.symmetric(vertical: 16.0),

            alignment: FractionalOffset.centerLeft,

// decoration: BoxDecoration(
// shape: BoxShape.circle,
// image: new DecorationImage(

// fit: BoxFit.fill,
// image: new AssetImage("assets/hope.jpg"),
// ),
// ),
            child: new Hero(
              tag: "planet-hero-${document['Name']}",
              child: new Image(
                image: new NetworkImage(document['image_url']),
                //centerSlice: new Rect.fromLTWH(50.0, 50.0, 82.0, 82.0),
                height: 82.0,
                width: 82.0,
              ),
            ),
          ),
        ]),
        onTap: () {
          // Navigator.of(contex).push(new MaterialPageRoute(
          //     builder: (context) => new CommiteeDetails(
          //         value: document['Name'],
          //         image: document['image_url'],
          //         bio: document['Bio'],
          //         value2: document['Designation'])));
        },
      ),
    ],
  );
}

