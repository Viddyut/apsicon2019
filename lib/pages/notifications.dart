import 'dart:async';
import 'dart:ui';
import 'package:apsicon2019/common/separator.dart';
import 'package:apsicon2019/dataholder/DBHelper.dart';
import 'package:apsicon2019/model/notification_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:onesignal/onesignal.dart';

Future<List<NotificationData>> getScheduleFromDB() async {
  var dbHelper = DBHelper();
  Future<List<NotificationData>> schedules = dbHelper.getSchedule();
  return schedules;
}

class Notifications extends StatefulWidget {
  @override
  _Notifications createState() => _Notifications();
}

class _Notifications extends State<Notifications> {

Widget _buildLocal(BuildContext context){
  return Container(
        height: 200.0,
        child: FutureBuilder<List<NotificationData>>(
            future: getScheduleFromDB(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );

              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemExtent: 80.0,
                  itemBuilder: (context, index) {
                    print(snapshot.data[index].title);
                    return Card(
                      elevation: 2.0,
                      child: ListTile(
                        trailing: GestureDetector(
                          child: new Icon(
                            Icons.notifications,
                            color: Theme.of(context).primaryColor,
                          ),
                          onTap: () {},
                        ),
                        title: Row(
                          children: [
                            Expanded(
                              child:
                                  Text(snapshot.data[index].title, maxLines: 2),
                            ),
                          ],
                        ),
                        subtitle:
                            Text(snapshot.data[index].body),
                        onTap: () {
                          print(snapshot.data[index].title);
                          //Navigator.of(context).push(new MaterialPageRoute(

                          // builder: (context) => new ProgramDetails(topic: snapshot.data[index].topic,
                          // date: snapshot.data[index].date,
                          // day: snapshot.data[index].day,
                          // time: snapshot.data[index].time,
                          // session: snapshot.data[index].session,
                          // panelist: snapshot.data[index].panelist,
                          // chairperson: snapshot.data[index].chairperson,
                          // speaker: snapshot.data[index].speaker,
                          // moderator: snapshot.data[index].moderator,
                          // session_coordinator: snapshot.data[index].session_coordinator,
                          // abstracts: snapshot.data[index].abstracts,
                          // )));
                        },
                      ),
                    );
                  });
            }),
      );
}

Widget _buildRemote(BuildContext context){
  return Container(
        decoration: BoxDecoration(
          image: DecorationImage(image:AssetImage('assets/lord.jpg'), fit: BoxFit.fill ) 
        ),
        child: Stack(
          children: <Widget>[
             Positioned(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 2.5, sigmaY: 2.5),
                                        child: Container(
                                          color: Colors.black.withOpacity(0),
                                        ),
                                      ),
                                    ),
            StreamBuilder(
                  stream: Firestore.instance.collection('notifications').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    return ListView.builder(
                     
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) =>
                          _notifications(context, snapshot.data.documents[index]),
                      
                    );
                  },
                ),
          ],
        ),
      );
}

  @override
  Widget build(BuildContext context) {
    OneSignal.shared.setNotificationReceivedHandler((notification) {
      build(context);
    });
    
    return Scaffold(
    
      appBar: AppBar(title: Text('Notifications'),centerTitle: true,),
      body:
      // _buildLocal(context),
      _buildRemote(context),
    );
  }

  Widget _notifications(BuildContext context,DocumentSnapshot document){
   
     return Column(
      
      children: <Widget>[
GestureDetector(
  child: Card(

            color: Theme.of(context).primaryColorLight,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            margin: new EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.only(left: 5.0, top: 5, bottom: 5,right: 5.0),
              height: 75.0,
              child: ListTile( 
                title: Row(
                  children: [
                    Expanded(
                      child: Text(document['Title'], style: TextStyle(color: Colors.black),),
                    ),
                  ],
                ),
                subtitle: Column(
                  children: <Widget>[
                    Container(alignment: Alignment(-1, 0), child: Separator()),
                    Container(alignment: Alignment(-1, 0), child: Text(document['Body'], style: TextStyle(color: Colors.black),)),
                  ],
                ),
              ),
            ),
          ),
)
      ],
    );
  }
}
