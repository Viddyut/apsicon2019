import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Venue extends StatefulWidget {
  @override
  _Venue createState() => _Venue();
}

class _Venue extends State<Venue> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(20.285374, 85.773463),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      //bearing: 192.8334901395799,
      target: LatLng(20.285374, 85.773463),
      tilt: 0.0,
      zoom: 12
      );

  @override
  void initState() {
    super.initState();
    _goToTheLake();
  }

  @override
  Widget build(BuildContext context) {
    double width = (MediaQuery.of(context).size.width);
    double height = (MediaQuery.of(context).size.height);

    return Scaffold(
      appBar: AppBar(
        title: Text("Venue Details"),
      ),
      body: Container(
          color: Theme.of(context).primaryColorLight,
          height: height,
          width: width,
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 8,
                child: GoogleMap(
                  markers: {mark},
                  mapType: MapType.terrain,
                  initialCameraPosition: _kGooglePlex,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ),
              ),
              Flexible(
                flex: 2,
                child: Container(
                  padding: const EdgeInsets.all(5.0),
                  width: width,
                  child: Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 5.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                              "APSICON 2019\nSiksha 'O' Anusandhan University Campus,\nKhandagiri Marg, 2, Sum Hospital Rd,\nBhubaneswar,Odisha, India-751030",
                              style: TextStyle(
                                  fontSize: 15,
                                  color: Theme.of(context).primaryColor),
                              textAlign: TextAlign.center),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  Marker mark = Marker(
      markerId: MarkerId('Siksha O Anusandhan'),
      position: LatLng(20.285374, 85.773463),
      infoWindow: InfoWindow(title: 'APSICON 2019, SOA Campus'),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));
}
