import 'package:apsicon2019/detail/social_details.dart';
import 'package:apsicon2019/pages/konark_map.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SocialEvents extends StatefulWidget {

  @override
  _SocialEvents createState() => _SocialEvents();
  }
  
  class _SocialEvents extends State<SocialEvents> with SingleTickerProviderStateMixin {
     TabController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
   
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text('Social Events'),centerTitle: true,
      actions: <Widget>[
        Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 5.0, 0),
              child: GestureDetector(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 8,
                    ),
                    Icon(
                      Icons.map,
                      color: Colors.orange[800],
                    ),
                    Text(
                      "Sun Temple\nKonark Map",
                      style: TextStyle(fontSize: 8.0, color: Colors.orange[800]),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context) => new KonarkMap()));
                },
              )),
      ],),
      body:TabBarView(
        controller: controller,
        children: <Widget>[
         _list("socialprogramday1"),
         _list("socialprogramday2"),
         _list("socialprogramday3"),
         _list("socialprogramday4"),

        ],
      ),

      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: new TabBar(
           unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: controller,
          indicatorWeight: 4.0,          
          indicator: BoxDecoration(color: Colors.white,
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15.0),bottomRight:  Radius.circular(15.0))),
          tabs: <Tab>[new Tab(text: "11 Dec"), new Tab(text: "12 Dec"),new Tab(text: "13 Dec"),new Tab(text: "1 Dec"),],
        ),
      ),
    );
  }
  Widget _list(String day) {
    return Container(
      child: StreamBuilder(
        stream: Firestore.instance
            .collection(day)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          return ListView.builder(
            shrinkWrap: false,
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) =>
                _buildProgramDay1(context, snapshot.data.documents[index]),
            padding: const EdgeInsets.only(top: 10),
          );
        },
      ),
    );
  }
  Widget _buildProgramDay1(BuildContext contex, DocumentSnapshot document) {
    return Container(
      color: Theme.of(context).primaryColorLight,
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Stack(children: <Widget>[
              Card(
                //color: Colors.white,
                margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 2.0),
                elevation: 0.0,
                child: Container(
                  padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                  height: 120.0,
                  child: ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${document['type'].toString().replaceAll("\\n", "\n")}",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w500),maxLines: 2,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "${document['topic'].toString().replaceAll("\\n", "\n")}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        document['time'] != ""
                            ? Row(
                                children: <Widget>[
                                  Flexible(
                                    flex: 1,
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.access_time,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.fromLTRB(
                                              5.0, 0, 0, 0),
                                          child: Text(
                                            document['time'],
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontStyle: FontStyle.italic),
                                          ),
                                          alignment: Alignment(-1, 0),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Flexible(
                                    flex: 1,
                                    child: Container(),
                                  )
                                ],
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
            onTap: () {
            if(document['img_url']!=""){
              Navigator.of(context).push(new MaterialPageRoute(
                    builder: (context) => new SocialDetails(
                        img: document['img_url'],
                        topic: document['topic'],
                        type: document['type'],
                        time: document['time'],
                        details: document['details'],
                        )));
            }
            },
          ),
        ],
      ),
    );
  }
}