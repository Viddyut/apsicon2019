import 'package:apsicon2019/pages/floor_map.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Sponsors extends StatefulWidget{

  @override
  _Sponsors createState()=>_Sponsors();
  }
  
  class _Sponsors extends State<Sponsors> with SingleTickerProviderStateMixin{
   TabController _controller;

   @override
  void initState() {
    super.initState();
    _controller = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(title: Text("Trade"),centerTitle: true,),
      body: TabBarView(
        controller: _controller,
        children: <Widget>[
          Container(
          child: Container(
            child: StreamBuilder(
              stream: Firestore.instance.collection('trade_partners').orderBy('preference').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                return ListView.builder(
                  shrinkWrap: false,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) =>
                      _buildTradePartners(context, snapshot.data.documents[index]),
                  padding: const EdgeInsets.only(top: 10),
                );
              },
            ),
          ),
        ),
        FloorPlan(),
        ],
               
      ),
      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: new TabBar(
           unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: _controller,
          indicatorWeight: 4.0,          
          indicator: BoxDecoration(color: Colors.white,
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15.0),bottomRight:  Radius.circular(15.0))),
          tabs: <Tab>[new Tab(text: "Trade Partners"), new Tab(text: "Trade Layout")],
        ),
      ),
    );
  }

  Widget _buildTradePartners(BuildContext context, DocumentSnapshot document){
    return Card(
            color: Theme.of(context).primaryColor,
            
            margin: new EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
            elevation: 5.0,
            child: Container(
              //padding: EdgeInsets.only(left: 40.0, top: 5, bottom: 10),
              height: 200.0,
              child: new FadeInImage(
        placeholder: new AssetImage('assets/logo.png'),
        image: new NetworkImage(document['img_url']),
        fit: BoxFit.fill,
        //alignment: Alignment.center,
        fadeInDuration: new Duration(milliseconds: 200),
        fadeInCurve: Curves.linear,
      ),
            ),
          );
  }
}