import 'dart:ui';

import 'package:apsicon2019/common/separator.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Commitee extends StatefulWidget {
  @override
  _Commitee createState() => new _Commitee();
}

class _Commitee extends State<Commitee> {
  @override
  Widget build(BuildContext context) {
 
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(title: Text('Oraganizing Commitee'),centerTitle: true,),
        body: 
        Container(
            decoration: BoxDecoration(
          image: DecorationImage(image:AssetImage('assets/lord.jpg'), fit: BoxFit.fill ) 
        ),
            padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 14.0),
     

            child: Stack(

              children: <Widget>[
                Positioned(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 2.5, sigmaY: 2.5),
                                        child: Container(
                                          color: Colors.black.withOpacity(0),
                                        ),
                                      ),
                                    ),
                StreamBuilder(
                  stream: Firestore.instance
                      .collection('committee')
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    return ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) =>
                          _buildListItem(context, snapshot.data.documents[index]),
                    );
                  },
                ),
              ],
            ),
          ),

            );
  }

  Widget _buildListItem(BuildContext contex, DocumentSnapshot document) {
  return Container(
    
    child: Column(
      children: <Widget>[
        GestureDetector(
          child: Stack(children: <Widget>[
            Card(
              
              color: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                
                  borderRadius: BorderRadius.circular(15.0)),
              margin: new EdgeInsets.fromLTRB(46.0, 0.0, 6.0, 15.0),
              elevation: 5.0,
              child: Container(
                padding: EdgeInsets.only(left: 40.0, top: 5, bottom: 10),
                height: 118.0,
                child: ListTile(
                  title: Row(
                    children: [
                      Expanded(
                        child: Text(document['Name'],style: TextStyle(color: Colors.white),),
                      ),
                    ],
                  ),
                  subtitle: Column(
                    children: <Widget>[
                      Container(alignment: Alignment(-1, 0), child: Separator()),
                      Container(child: Text(document['Designation'], style: TextStyle(color: Colors.white),), alignment: Alignment(-1, 0),),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 16.0),

              alignment: FractionalOffset.centerLeft,

// decoration: BoxDecoration(
// shape: BoxShape.circle,
// image: new DecorationImage(

// fit: BoxFit.fill,
// image: new AssetImage("assets/hope.jpg"),
// ),
// ),
              child: new Hero(
                tag: "planet-hero-${document['Name']}",
                
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle
                  ),
                  child: new CircleAvatar(
                      radius: 45.0,
                      backgroundImage:
                          
                          new NetworkImage(document['image_url']),
                      backgroundColor: Colors.transparent,
                      
                    ),
                ),
              ),
            ),
          ]),
          onTap: () {
            // Navigator.of(contex).push(new MaterialPageRoute(
            //     builder: (context) => new CommiteeDetails(
            //         value: document['Name'],
            //         image: document['image_url'],
            //         bio: document['Bio'],
            //         value2: document['Designation'])));
          },
        ),
      ],
    ),
  );
}
}
