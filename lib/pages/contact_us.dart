import 'package:flutter/material.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUs createState() => _ContactUs();
}

class _ContactUs extends State<ContactUs> {
  final color = const Color(0xFFC2E7EF);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      appBar: AppBar(
        title: Text('Contact Us'),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          color: color,
          padding: const EdgeInsets.symmetric(horizontal: 5.0),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              SizedBox(
                height: 20.0,
              ),
              Card(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                elevation: 10.0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 5.0,
                      ),
                      Icon(Icons.contact_phone,
                          color: Theme.of(context).primaryColor),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text("Local Organising Secretariat\n",
                          style: TextStyle(
                              fontSize: 18,
                              color: Theme.of(context).primaryColor),
                          textAlign: TextAlign.center),
                      //Divider(height: 2.0,indent: 5.0,endIndent: 5.0,),
                      Text(
                        "Dr. Bibhuti Bhusan Nayak\n\nSBM Hospital Pvt. Ltd. At-Balikuda,Po-Gopalpur, Near Level Crossing, Dist-Cuttack, Pin-753011\n\nMobile: +91 94370 64487\nEmail: bibhutinayak50@gmail.com \n",
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center,
                      ),
                      Divider(
                        height: 2.0,
                        indent: 5.0,
                        endIndent: 5.0,
                        color: Theme.of(context).primaryColor,
                      ),
                      //  SizedBox(
                      //   height: 5.0,
                      // ),
                      // Icon(Icons.contact_phone,color: Theme.of(context).primaryColor,),

                      SizedBox(
                        height: 5.0,
                      ),
                      Text("CONFERENCE SECRETARIAT\n",
                          style: TextStyle(
                              fontSize: 18,
                              color: Theme.of(context).primaryColor),
                          textAlign: TextAlign.center),

                      Text("Mr. Devraj Chhaperwal - 9820392657\n\n",
                          style: TextStyle(fontSize: 15),
                          textAlign: TextAlign.center),
                      Divider(
                          height: 2.0,
                          indent: 5.0,
                          endIndent: 5.0,
                          color: Theme.of(context).primaryColor),
                      // SizedBox(
                      //   height: 5.0,
                      // ),
                      // Icon(Icons.location_on,
                      //     color: Theme.of(context).primaryColor),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text("Professional Conference Organisers\n",
                          style: TextStyle(
                              fontSize: 18,
                              color: Theme.of(context).primaryColor),
                          textAlign: TextAlign.center),
                      //Divider(height: 2.0,indent: 5.0,endIndent: 5.0,),
                      Text(
                        "River Route Creative Group\n\nUnit No.9, Cama Industrial Premises Co. Op.Soc. Ltd, Sunmill Compound, Lower Parel Mumbai-400013\n",
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
              //   SizedBox(
              //    height: 20.0,
              //  ),
              //   Card(
              //     margin: const EdgeInsets.symmetric(horizontal: 20),
              //     elevation: 10.0,
              //     child: Column(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: <Widget>[

              //       ],
              //     ),
              //   ),
              //   SizedBox(
              //     height: 20.0,
              //   ),
              //   Card(
              //     margin: const EdgeInsets.symmetric(horizontal: 20),
              //     elevation: 10.0,
              //     child: Column(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: <Widget>[
              //         SizedBox(
              //           height: 5.0,
              //         ),
              //         Icon(Icons.location_on,color: Theme.of(context).primaryColor),
              //         SizedBox(
              //           height: 5.0,
              //         ),
              //         Text("Professional Conference Organisers\n",
              //             style: TextStyle(fontSize: 18,color: Theme.of(context).primaryColor),
              //             textAlign: TextAlign.center),
              //             //Divider(height: 2.0,indent: 5.0,endIndent: 5.0,),
              //         Text(
              //           "River Route Creative Group\n\nUnit No.9, Cama Industrial Premises Co. Op.Soc. Ltd, Sunmill Compound, Lower Parel Mumbai-400013\n",
              //           style: TextStyle(fontSize: 15),
              //           textAlign: TextAlign.center,
              //         ),
              //       ],
              //     ),
              //   ),

              SizedBox(
                height: 20.0,
              ),
              Card(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                elevation: 10.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 5.0,
                    ),
                    Icon(Icons.drive_eta,
                        color: Theme.of(context).primaryColor),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text("Logistics\n",
                        style: TextStyle(
                            fontSize: 20,
                            color: Theme.of(context).primaryColor),
                        textAlign: TextAlign.center),
                    //Divider(height: 2.0,indent: 5.0,endIndent: 5.0,),
                    Text(
                      "Mr. Sachin Shahane - 9890909494\nEmail: rrcg.sachin@gmail.com\n",
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),

              SizedBox(
                height: 20.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
