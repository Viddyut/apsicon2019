import 'package:apsicon2019/detail/program_details.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SubProgram extends StatefulWidget {
  String day;

  SubProgram({Key key, this.day}) : super(key: key);

  @override
  _SubProgramState createState() => _SubProgramState();
}

class _SubProgramState extends State<SubProgram> with TickerProviderStateMixin {
  TabController ncontroller;

  @override
  void initState() {
    super.initState();
    ncontroller = new TabController(vsync: this, length: 5);
  }

  @override
  void dispose() {
    ncontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                //color: Theme.of(context).primaryColor,
                border: Border.all(color: Theme.of(context).primaryColor),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5.0),
                    topRight: Radius.circular(5.0),
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0))),
            child: TabBar(
              indicatorColor: Colors.teal,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.black54,
              isScrollable: true,
              controller: ncontroller,
              indicatorWeight: 4.0,
              indicator: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  border: Border.all(color: Theme.of(context).primaryColor),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.0),
                      topRight: Radius.circular(5.0),
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0))),
              tabs: <Tab>[
                new Tab(
                  child:Text("Hall A\nKonark\n(Plenary Sessions)",textAlign: TextAlign.center,style: TextStyle(fontSize: 12),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                  new Tab(
                  child:Text("Hall B\nBarabati",textAlign: TextAlign.center,style: TextStyle(fontSize: 12),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                   new Tab(
                  child:Text("Hall C\nChilika",textAlign: TextAlign.center,style: TextStyle(fontSize: 12),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                   new Tab(
                  child:Text("Hall D\nDhauli",textAlign: TextAlign.center,style: TextStyle(fontSize: 12),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                   new Tab(
                  child:Text("Hall E\nAshoka",textAlign: TextAlign.center,style: TextStyle(fontSize: 12),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
               
              ],
            ),
          ),
          Container(
            height: screenHeight * 0.75,
            //margin: EdgeInsets.only(left: 16.0, right: 16.0),
            child: TabBarView(
              controller: ncontroller,
              children: <Widget>[
                _list('Hall A'),
                _list('Hall B'),
                _list('Hall C'),
                _list('Hall D'),
                _list('Hall E'),
                //Container(),
              ],
            ),
          ),
        ]);
  }

  Widget _buildProgramDay1(BuildContext contex, DocumentSnapshot document) {
    return Container(
      color: Theme.of(context).primaryColorLight,
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Stack(children: <Widget>[
              Card(
                //color: Colors.white,
                margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 2.0),
                elevation: 0.0,
                child: Container(
                  padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                  height: 120.0,
                  child: ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${document['type']}",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w500),maxLines: 1,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "${document['topic'].toString().replaceAll("\\n", "\n")}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        document['Time'] != ""
                            ? Row(
                                children: <Widget>[
                                  Flexible(
                                    flex: 1,
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.access_time,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.fromLTRB(
                                              5.0, 0, 0, 0),
                                          child: Text(
                                            document['time'],
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontStyle: FontStyle.italic),
                                          ),
                                          alignment: Alignment(-1, 0),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Flexible(
                                    flex: 1,
                                    child: 
                                    document['speakers']!=""?
                                    Row(
                                      
                                      children: <Widget>[
                                        Icon(
                                          Icons.mic_none,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.fromLTRB(
                                              5.0, 0, 0, 0),
                                          child: Text(
                                            document['speakers']
                                                .toString()
                                                .replaceAll("\\n", "\n"),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontStyle: FontStyle.italic,fontSize: 14),maxLines: 3,
                                          ),
                                          alignment: Alignment(-1, 0),
                                        ),
                                      ],
                                    ):Row(),
                                  )
                                ],
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
            onTap: () {
              if (document['type'] != "") {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (context) => new ProgramDetails(
                        id: document['alertid'],
                        topic: document['topic'],
                        type: document['type'],
                        hall: document['hall'],
                        time: document['time'],
                        alerttime: document['alerttime'],
                        date: document['date'],
                        alertdate: document['alertdate'],
                        chairperson: document['chairpersons'],
                        moderator: document['moderators'],
                        speakers: document['speakers'])));
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _list(String hall) {
    return Container(
      child: StreamBuilder(
        stream: Firestore.instance
            .collection(widget.day)
            .where('hall', isEqualTo: hall)
            .orderBy('alertid')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          return ListView.builder(
            shrinkWrap: false,
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) =>
                _buildProgramDay1(context, snapshot.data.documents[index]),
            padding: const EdgeInsets.only(top: 10),
          );
        },
      ),
    );
  }
}
