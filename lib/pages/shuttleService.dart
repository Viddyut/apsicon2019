import 'package:apsicon2019/pages/routes.dart';
import 'package:flutter/material.dart';

class ShuttleServices extends StatefulWidget {
  @override
  _ShuttleServicesState createState() => _ShuttleServicesState();
}

class _ShuttleServicesState extends State<ShuttleServices> with SingleTickerProviderStateMixin {

   TabController controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
      appBar: AppBar(title: Text('Shuttle Services'),centerTitle: true,),
      body:TabBarView(
        controller: controller,
        children: <Widget>[
        Routes(routeNum: 1,),
        Routes(routeNum: 2,),
        Routes(routeNum: 3,),
        Routes(routeNum: 4,),
       

        ],
      ),

      bottomNavigationBar: Material(
        
        color: Theme.of(context).primaryColor,
        child: new TabBar(
          
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: controller,
          indicatorWeight: 4.0,          
          indicator: BoxDecoration(color: Colors.white,
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5.0),bottomRight:  Radius.circular(5.0))),
          tabs: <Tab>[
                  new Tab(
                   child:Text("Route 1\nTrident &\nGinger",textAlign: TextAlign.center,style: TextStyle(fontSize: 11),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                  new Tab(
                  child:Text("Route 2\nPresidency &\nCrown",textAlign: TextAlign.center,style: TextStyle(fontSize: 11),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                  new Tab(
                  child:Text("Route 3\nSISMO &\nPantha Nivas",textAlign: TextAlign.center,style: TextStyle(fontSize: 11),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
                  new Tab(
                  child:Text("Route 4\nExcellency",textAlign: TextAlign.center,style: TextStyle(fontSize: 11),) ,
                  //text: "Hall A\nPleanar Sessions",
                  ),
            ],
        ),
      ),
    );
  }
}