import 'package:flutter/material.dart';

class AccompProgram extends StatefulWidget {
  @override
  _AccompProgramState createState() => _AccompProgramState();
}

class _AccompProgramState extends State<AccompProgram> with SingleTickerProviderStateMixin {
  TabController _controller;
   @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = (MediaQuery.of(context).size.height);
    return Scaffold(
      appBar: AppBar(title: Text("Accompanying Person Program"),),
      body: TabBarView(
        controller: _controller,
        children: <Widget>[
_programLayout(height,"Local Temples & Dhawalgiri","assets/dhavali.jpg","1:30 PM","Near SOA Auditorium","4:30 PM","Near SOA Auditorium","Kindly Contact Register Desk to register for accompaying person program or mail to rrcg.sachin@gmail.com"),
_programLayout(height,"Sun Temple Konark","assets/suntemple.jpg","12:30 PM","Near SOA Auditorium","8:30 PM onwards","Respected Hotels","Kindly Contact Register Desk to register for accompaying person program or mail to rrcg.sachin@gmail.com"),
_programLayout(height,"Lord Jagganath Puri","assets/puri.jpg","9:00 AM","Near SOA Auditorium","3:30 PM onwards","Near SOA Auditorium","Kindly Contact Register Desk to register for accompaying person program or mail to rrcg.sachin@gmail.com"),
_programLayout(height,"Chilika Lake","assets/chilika.jpg","9:00 AM","Near SOA Auditorium","3:30 PM onwards","Near SOA Auditorium",
"For 'Chilika Lake' additional 300/person fees will be charged by OTDC for boating.Amount will be collected in bus when traveling towars Chilika Lake.\n\nKindly Contact Register Desk to register for accompaying person program or mail to rrcg.sachin@gmail.com"),        ],
      ),
      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: new TabBar(
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: _controller,
          indicatorWeight: 4.0,
          indicator: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(5.0),
                  bottomRight: Radius.circular(5.0))),
          tabs: <Tab>[
            new Tab(text: "11 Dec"),
            new Tab(text: "12 Dec"),
            new Tab(text: "13 Dec"),
            new Tab(text: "14 Dec"),
            
          ],
        ),
      ),
      );  
      
  }

  Widget _programLayout(double height, String destination,String img,String fromSOA,String pickup,String toSOA,String drop,String note){
    return Container(
      
child: ListView(
  shrinkWrap: true,
  children: <Widget>[
     Container(
              color: Theme.of(context).primaryColor,
              margin: const EdgeInsets.only(top: 0.0),
              padding:
                  const EdgeInsets.only(left: 10.0, bottom: 10.0, top: 10.0),
              child: Text(
                destination,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ),
    Image(
      image: AssetImage(img), fit: BoxFit.fill,
      height:  height>950?400:200,
    ),
      DataTable(
        columns: [
          DataColumn(label: Text('Departure from SOA:',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 14),)),
          DataColumn(label: Text(fromSOA,)),
        ],
        rows: [
          DataRow(cells: [
            DataCell( Text('Pickup Point:',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
            DataCell( Text(pickup,)),
          ]),
          DataRow(cells: [
            DataCell( Text('Departure towards SOA:',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
            DataCell( Text(toSOA)),
          ]),
          DataRow(cells: [
            DataCell( Text('Drop Point:',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
            DataCell( Text(drop,)),
          ]),
         
        ],
      ),
SizedBox(height: 20.0,),
Padding(
        padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Text("Note:",textAlign: TextAlign.justify,style: TextStyle(color: Colors.black),),
      ),
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(note,textAlign: TextAlign.justify,style: TextStyle(color: Theme.of(context).primaryColor),),
      ),
    
    
  ],
),

    );
  }
}