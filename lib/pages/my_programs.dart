import 'package:apsicon2019/detail/program_details.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class MyPrograms extends StatefulWidget {
  final String name;
  MyPrograms({Key key, this.name}) : super(key: key);

  @override
  _MyProgramsState createState() => _MyProgramsState();
}

class _MyProgramsState extends State<MyPrograms> with TickerProviderStateMixin {

TabController _controller;

@override
void initState(){
  super.initState();
_controller = new TabController(vsync: this, length: 5);
}

@override
void dispose(){
_controller.dispose();
  super.dispose();
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Programs"),
        centerTitle: true,
      ),
      body: TabBarView(

        controller: _controller,
        children: <Widget>[
          _list("programday1final"),
          _list("programday1fina2"),
          _list("programday1fina3"),
          _list("programday1fina4"),
          _list("programday1fina5"),
        ],
      ),

      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        
        child: new TabBar(
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black,
          controller: _controller,
          indicatorPadding: const EdgeInsets.only(top: 0.0),         
          indicator: BoxDecoration(color: Colors.white),
          tabs: <Tab>[
            new Tab(text: "11 Dec"), 
            new Tab(text: "12 Dec"),
            new Tab(text: "13 Dec"), 
            new Tab(text: "14 Dec"),
            new Tab(text: "15 Dec"), 
            
            ],
        ),
      ),
      
    );
  }

   Widget _list(String day){
  return  Container(
          child: StreamBuilder(
            stream: Firestore.instance.collection(day).where('speaker', arrayContains: widget.name).snapshots(),
            
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );
              return ListView.builder(
                shrinkWrap: false,
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) =>
                    _buildProgramDay1(context, snapshot.data.documents[index]),
                padding: const EdgeInsets.only(top: 10),
              );
            },
          ),
        );  
}

Widget _buildProgramDay1(BuildContext contex, DocumentSnapshot document) {
    
    return Container(
      color: Theme.of(context).primaryColorLight,
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Stack(children: <Widget>[
              Card(
                //color: Colors.white,
                margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 2.0),
                elevation: 0.0,
                child: Container(
                  padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                  height: 110.0,
                  child: ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${document['hall']}",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.w500),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "${document['topic']}",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),

                        document['Time']!=""?
                        Row(
                          children: <Widget>[
                            
                            Icon(
                              Icons.access_time,
                              color: Theme.of(context).primaryColor,
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(5.0, 0, 0, 0),
                              child: Text(
                                document['time'],
                                style: TextStyle(
                                    color: Colors.black,
                                    fontStyle: FontStyle.italic),
                              ),
                              alignment: Alignment(-1, 0),
                            ),
                          ],
                        ):Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ]),
            onTap: () {
              
              if (document['type'] != "") {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (context) => new ProgramDetails(
                        id: document['alertid'],
                        topic: document['topic'],
                        type: document['type'],
                        hall: document['hall'],
                        time: document['time'],
                        alerttime: document['alerttime'],
                        date: document['date'],
                        alertdate: document['alertdate'],
                        chairperson: document['chairpersons'],
                        moderator: document['moderators'],
                        speakers: document['speakers']
                          
                                                  )));
              }
            },
          ),
        ],
      ),
    );
  }
}