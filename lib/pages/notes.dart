import 'package:apsicon2019/dataholder/DBHelper_notes.dart';
import 'package:apsicon2019/detail/note_details.dart';
import 'package:apsicon2019/model/notes_data.dart';
import 'package:flutter/material.dart';

Future<List<NotesData>> getScheduleFromDB() async {
  var dbHelper = DBHelperNotes();
  Future<List<NotesData>> schedules = dbHelper.getNotes();
  return schedules;
}

class Note extends StatelessWidget {
  // Note({this.noteText});
  // final String noteText;

  @override
  Widget build(BuildContext context) {
    
    Container(
      margin: const EdgeInsets.symmetric(horizontal: 16.0),
      height: 300.0,
      child: FutureBuilder<List<NotesData>>(
          future: getScheduleFromDB(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(
                child: CircularProgressIndicator(),
              );

            return ListView.builder(
                shrinkWrap: true,
                itemCount: snapshot.data.length,
                itemExtent: 80.0,
                itemBuilder: (context, index) {
                  print(snapshot.data[index].note);

                  return Card(
                    elevation: 2.0,
                    child: ListTile(
                      trailing: GestureDetector(
                        child: new Icon(
                          Icons.notifications,
                          color: Theme.of(context).primaryColor,
                        ),
                        onTap: () {},
                      ),
                      title: Row(
                        children: [
                          Expanded(
                            child: Text(snapshot.data[index].note, maxLines: 2),
                          ),
                        ],
                      ),
                      onTap: () {
                        print(snapshot.data[index].note);
                      },
                    ),
                  );
                });
          }),
    );
  }
}

class Notes extends StatefulWidget {
  @override
  _Notes createState() => new _Notes();
}

class _Notes extends State<Notes> {
  //Create the list of notes to add to
  var dbHelper = DBHelperNotes();

  //final List<Note> _notes = <Note>[];
  final TextEditingController _textController = new TextEditingController();

  void _handleSubmitted(String text) {
    _textController.clear();

    var schedule = NotesData();
    schedule.note = text;

    dbHelper.addToNotes(schedule);
    setState(() {
      // _notes.insert(
      //     0,
      //     new Note(
      //         //noteText: text,
      //         ));
    });
  }

  void _clearAll() {
    setState(() {
      // _notes.clear();
    });
  }

  Widget _addNote() {
    return new IconTheme(
      data: new IconThemeData(color: Colors.white
          //Theme.of(context).primaryColor

          ),
      child: new Container(
          decoration: new BoxDecoration(
            border: Border.all(
                color: Theme.of(context).primaryColor,
                style: BorderStyle.solid,
                width: 1.0),
            borderRadius: BorderRadius.circular(200.0),
            color: Colors.white,
          ),
          margin: const EdgeInsets.symmetric(horizontal: 10.0),
          child: new Row(children: <Widget>[
            new Flexible(
              child: Container(
                padding: const EdgeInsets.only(left: 10.0),
                child: new TextField(
                  controller: _textController,
                  decoration:
                      new InputDecoration.collapsed(hintText: "Write a note"),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(5.0),
              child: new FloatingActionButton(
                  onPressed: () => _handleSubmitted(_textController.text),
                  tooltip: 'Create New Note',
                  child: new Icon(Icons.add)),
            )
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    var dbHerper = new DBHelperNotes();
    return new Scaffold(
      appBar:
          new AppBar(
            title: new Text("Notes"),
            elevation: 4.0,
          ),
      //     RoundedAppBar(
      //   title: "Notes",
      // ),
      body: new Container(
        margin: const EdgeInsets.all(10.0),
          child: new Column(
        children: <Widget>[
          // new RaisedButton(
          //     //heroTag: "adc notes",
          //     //onPressed: _notes.isEmpty ? null : () => _clearAll(),
          //     shape: RoundedRectangleBorder(),
          //     //backgroundColor: Colors.red,
          //     //tooltip: 'Clear Notes',
          //     child: new Text("Clear all notes")),
          new Flexible(
            child:
                //     new ListView.builder(
                //   padding: new EdgeInsets.all(2.0),
                //   itemBuilder: (_, int index) => _notes[index],
                //   itemCount: _notes.length,
                // )
                FutureBuilder<List<NotesData>>(
                    future: getScheduleFromDB(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return Center(
                          child: CircularProgressIndicator(),
                        );

                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemExtent: 80.0,
                          itemBuilder: (context, index) {
                            print(snapshot.data[index].note);

                            return Card(
                              elevation: 2.0,
                              child: ListTile(
                                trailing: GestureDetector(
                                  child: new Icon(
                                    Icons.delete_forever,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  onTap: () {
                                  
                                    //Alert Dialog
                                    return showDialog(
                                        context: context,
                                        builder: (BuildContext context) {

                                          Widget deleteButton = FlatButton(
                                            child: Text("Delete"),
                                            onPressed: () {
                                              dbHelper.deleteFromNotes(
                                                  snapshot.data[index]);
                                              setState(() {});
                                              Navigator.pop(context);
                                            },
                                          );

                                          Widget cancelButton = FlatButton(
                                            child: Text("Cancel"),
                                            onPressed: () {                                            
                                              setState(() {});
                                              Navigator.pop(context);
                                            },
                                          );

                                          return AlertDialog(
                                            
                                            actions: <Widget>[
                                              cancelButton,
                                              deleteButton
                                            ],

                                            title: Container(
                                              child: Text("Alert",style: TextStyle(color: Colors.black)),
                                              ),
                                       
                                            contentPadding: EdgeInsets.only(top: 20.0),

                                            content: Container(
                                              width: 400.0,
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  
                                                  SizedBox(height: 10.0,),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                        top: 5.0,
                                                           left: 25.0,
                                                          right: 30.0),
                                                      child: Text(
                                                        "Selected note will be deleted",
                                                      )),
                                                 
                                                ],
                                              ),
                                            ),
                                          );
                                        });
                                  },
                                ),
                                title: Row(
                                  children: [
                                    Expanded(
                                      child: Text(snapshot.data[index].note,
                                          maxLines: 1),
                                    ),
                                  ],
                                ),
                                subtitle: Text(
                                  snapshot.data[index].content,
                                  maxLines: 1,
                                ),
                                onTap: () {
                                  print(snapshot.data[index].note);
                                   Navigator.of(context).push(
              new MaterialPageRoute(builder: (context) => new NotesDetails(
                id: snapshot.data[index].id,
                title:snapshot.data[index].note,
                content:snapshot.data[index].content,
                type: "Edit",
                )));
                                },
                              ),
                            );
                          });
                    }),
          ),
         

        ],
      )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(
              new MaterialPageRoute(builder: (context) => new NotesDetails()));
        },
      ),
    );
  }

}
