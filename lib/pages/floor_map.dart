import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FloorPlan extends StatefulWidget {
  @override
  _FloorPlanState createState() => _FloorPlanState();
}

class _FloorPlanState extends State<FloorPlan> {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorLight,
      //appBar: AppBar(title: Text('Trade Layout'),centerTitle: true,),
      body: 
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
        child: Column(children: <Widget>[
Flexible(
            flex: 1,
                    child: PhotoView(
backgroundDecoration: BoxDecoration(
  color: Theme.of(context).primaryColorLight,
),
            //imageProvider: NetworkImage("https://www.apsicon2019.in/assets/images/trade-layout.jpg"),
            imageProvider: AssetImage("assets/trade1.jpeg"),

      ),
          ),
          Flexible(
            flex: 1,
                    child: PhotoView(
backgroundDecoration: BoxDecoration(
  color: Theme.of(context).primaryColorLight,
),
            //imageProvider: NetworkImage("https://www.apsicon2019.in/assets/images/trade-layout.jpg"),
            imageProvider: AssetImage("assets/trade2.jpeg"),

      ),
          ),
        ],
                   
        ),
      ),
    );
  }
}