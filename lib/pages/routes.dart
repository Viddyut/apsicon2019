import 'package:flutter/material.dart';

class Routes extends StatefulWidget {
  int routeNum;
  Routes({Key key, this.routeNum}) : super(key: key);
  @override
  _RoutesState createState() => _RoutesState();
}

class _RoutesState extends State<Routes> with SingleTickerProviderStateMixin {
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(vsync: this, length: 5);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 10.0),
            decoration: BoxDecoration(
                //color: Theme.of(context).primaryColor,
                border: Border.all(color: Theme.of(context).primaryColor),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5.0),
                    topRight: Radius.circular(5.0),
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0))),
            child: TabBar(
              
              indicatorColor: Colors.teal,
              labelColor: Colors.white,
              unselectedLabelColor: Colors.black54,
              isScrollable: true,
              controller: _controller,
              indicatorWeight: 4.0,
              indicator: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  border: Border.all(color: Theme.of(context).primaryColor),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5.0),
                      topRight: Radius.circular(5.0),
                      bottomLeft: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0))),
              tabs: <Tab>[
                new Tab(
                  child: Text(
                    "11 Dec",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  //text: "Hall A\nPleanar Sessions",
                ),
                new Tab(
                  child: Text(
                    "12 Dec",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  //text: "Hall A\nPleanar Sessions",
                ),
                new Tab(
                  child: Text(
                    "13 Dec",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  //text: "Hall A\nPleanar Sessions",
                ),
                new Tab(
                  child: Text(
                    "14 Dec",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  //text: "Hall A\nPleanar Sessions",
                ),
                new Tab(
                  child: Text(
                    "15 Dec",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  //text: "Hall A\nPleanar Sessions",
                ),
              ],
            ),
          ),
          widget.routeNum==1?
          Container(
            height: screenHeight * 0.75,
            //margin: EdgeInsets.only(left: 16.0, right: 16.0),
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
               

                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day1(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day2(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day3(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day4(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day5(),
                  ),
                ),
              ],
            ),
          ):Container(),
          widget.routeNum==2?
          Container(
            height: screenHeight * 0.75,
            //margin: EdgeInsets.only(left: 16.0, right: 16.0),
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
                
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day1(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day2(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day3(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day4(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day5(),
                  ),
                ),
              ],
            ),
          ):Container(),
          widget.routeNum==3?
          Container(
            height: screenHeight * 0.75,
            //margin: EdgeInsets.only(left: 16.0, right: 16.0),
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day1(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day2(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day3(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day4(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day5(),
                  ),
                ),
              ],
            ),
          ):Container(),
          widget.routeNum==4?
          Container(
            height: screenHeight * 0.75,
            //margin: EdgeInsets.only(left: 16.0, right: 16.0),
            child: TabBarView(
              controller: _controller,
              children: <Widget>[
               
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day1(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day2(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day3(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day4(),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: route1day5(),
                  ),
                ),
              ],
            ),
          ):Container(),
        ]);
  }

    Widget route1day1() {
    return Container(
      child: DataTable(
        columns: [
          DataColumn(label: Text('Departure\n(Hotel to SOA)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
          DataColumn(label: Text('Arrival\n(SOA to Hotel)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
        ],
        rows: [
          DataRow(cells: [
            DataCell(Text('7:00 AM')),
            DataCell(Text('8:30 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:30 AM')),
            DataCell(Text('9:00 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:00 AM')),
            DataCell(Text('9:30 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:30 AM')),
            DataCell(Text('10:30 PM')),
          ]),
        ],
      ),
    );
  }
    Widget route1day2() {
    return Container(
      child: DataTable(
        columns: [
          DataColumn(label: Text('Departure\n(Hotel to SOA)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),),),
          DataColumn(label: Text('Arrival\n(SOA to Hotel)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
        ],
        rows: [
          DataRow(cells: [
            DataCell(Text('7:00 AM')),
            DataCell(Text('')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:30 AM')),
            DataCell(Text('')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:00 AM')),
            DataCell(Text('')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:30 AM')),
            DataCell(Text('')),
          ]),
           DataRow(cells: [
            DataCell(Text('Departure to Konark',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
            DataCell(Text('Arrival to Hotel',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
          ]),
           DataRow(cells: [
            DataCell(Text('12:30 PM onwards')),
            DataCell(Text('8:30 PM onwards')),
          ]),
          
        ],
      ),
    );
  }
    Widget route1day3() {
    return Container(
      child: DataTable(
        columns: [
          DataColumn(label: Text('Departure\n(Hotel to SOA)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
          DataColumn(label: Text('Arrival\n(SOA to Hotel)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
        ],
        rows: [
          DataRow(cells: [
            DataCell(Text('6:30 AM')),
            DataCell(Text('8:30 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:00 AM')),
            DataCell(Text('9:00 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:30 AM')),
            DataCell(Text('9:30 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:00 AM')),
            DataCell(Text('10:30 PM')),
          ]),
        ],
      ),
    );
  }
    Widget route1day4() {
return Container(
      child: DataTable(
        columns: [
          DataColumn(label: Text('Departure\n(Hotel to SOA)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
          DataColumn(label: Text('Arrival\n(SOA to Hotel)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
        ],
        rows: [
          DataRow(cells: [
            DataCell(Text('6:30 AM')),
            DataCell(Text('8:30 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:00 AM')),
            DataCell(Text('9:00 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:30 AM')),
            DataCell(Text('9:30 PM')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:00 AM')),
            DataCell(Text('10:30 PM')),
          ]),
        ],
      ),
    );
  }
    Widget route1day5() {
    return Container(
      child: DataTable(
        columns: [
          DataColumn(label: Text('Departure\n(Hotel to SOA)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
          DataColumn(label: Text('Departure\n(SOA to AirPort)',style: TextStyle(color: Theme.of(context).primaryColor,fontSize: 15),)),
        ],
        rows: [
          DataRow(cells: [
            DataCell(Text('6:30 AM')),
            DataCell(Text('Shuttle Service')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:00 AM')),
            DataCell(Text('')),
          ]),
          DataRow(cells: [
            DataCell(Text('7:30 AM')),
            DataCell(Text('')),
          ]),
          DataRow(cells: [
            DataCell(Text('8:00 AM')),
            DataCell(Text('')),
          ]),
        ],
      ),
    );
  }

  
}
