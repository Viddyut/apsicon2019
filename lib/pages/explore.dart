import 'package:apsicon2019/common/separator.dart';
import 'package:apsicon2019/detail/explore_details.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:ui';
class Explore extends StatefulWidget {
  @override
  _Explore createState() => _Explore();
}

class _Explore extends State<Explore> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Explore More'),
        centerTitle: true,
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(image:AssetImage('assets/lord.jpg'), fit: BoxFit.fill ) 
        ),
        //color: Theme.of(context).primaryColorLight,
        padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 14.0),
        child: Stack(
          children: <Widget>[
            Positioned(
                                      top: 0,
                                      bottom: 0,
                                      left: 0,
                                      right: 0,
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 2.5, sigmaY: 2.5),
                                        child: Container(
                                          color: Colors.black.withOpacity(0),
                                        ),
                                      ),
                                    ),
            StreamBuilder(
            stream: Firestore.instance.collection('explore').snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );
              return ListView.builder(
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) =>
                    _explore(context, snapshot.data.documents[index]),
              );
            },
          ),
          ],
          
        ),
      ),
    );
  }

  Widget _explore(BuildContext context, DocumentSnapshot document) {
    return Column(
      children: <Widget>[
        GestureDetector(
          child: Stack(children: <Widget>[
            Card(
              color: Theme.of(context).primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)),
              margin: new EdgeInsets.fromLTRB(46.0, 0.0, 6.0, 15.0),
              elevation: 5.0,
              child: Container(
                padding: EdgeInsets.only(left: 40.0, top: 5, bottom: 10),
                height: 118.0,
                child: ListTile(
                  title: Row(
                    children: [
                      Expanded(
                        child: Text(
                          document['Title'],
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  subtitle: Column(
                    children: <Widget>[
                      Container(
                          alignment: Alignment(-1, 0), child: Separator()),
                      Container(
                          alignment: Alignment(-1, 0),
                          child: Text(
                            document['Info'],
                            style: TextStyle(color: Colors.white),maxLines: 2,
                          )),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              margin: new EdgeInsets.symmetric(vertical: 16.0),
              alignment: FractionalOffset.centerLeft,
              child: new Hero(
                tag: "${document['Title']}",
                child: new Image(
                  image: new NetworkImage(document['Img_url']),fit: BoxFit.fill,
                  height: 82.0,
                  width: 82.0,
                ),
              ),
            ),
          ]),
          onTap: () {
             Navigator.of(context).push(new MaterialPageRoute(

                builder: (context) => new ExploreDetails(
                      img: document['Img_url'],
                      info: document['Info'],
                      title:document['Title'] ,
                    )));
          },
        ),
      ],
    );
  }
}
