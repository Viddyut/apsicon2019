import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class KonarkMap extends StatefulWidget {
  @override
  _KonarkMapState createState() => _KonarkMapState();
}

class _KonarkMapState extends State<KonarkMap> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Sun Temple Konark Map"),),
      body:  Container(
       height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
        child: PhotoView(
backgroundDecoration: BoxDecoration(
  color: Theme.of(context).primaryColorLight,
),
              
              imageProvider: AssetImage("assets/konark_map.jpg"),

        ),
      ),
    );
  }
}