import 'dart:async';
import 'dart:typed_data';
import 'package:apsicon2019/pages/programs.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class ProgramDetails extends StatefulWidget {
  String id,
      hall,
      date,
      time,
      alertdate,
      alerttime,
      speakers,
      number,
      topic,
      chairperson,
      moderator,
      session,
      type;
  ProgramDetails(
      {Key key,
      this.id,
      this.hall,
      this.date,
      this.time,
      this.alertdate,
      this.alerttime,
      this.topic,
      this.chairperson,
      this.number,
      this.moderator,
      this.session,
      this.type,
      this.speakers})
      : super(key: key);
  @override
  _ProgramDetails createState() => _ProgramDetails();
}

class _ProgramDetails extends State<ProgramDetails> {
  var flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  //Dialog _reminderDiag;
  String speaker;
  String listhead = "Speakers \\ Faculty";
  String chairperson, moderator;

  @override
  void initState() {
    super.initState();
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    if (!(widget.speakers == null)) {
      speaker = widget.speakers.replaceAll("\\n", "\n");
    }
    if (!(widget.chairperson == null)) {
      chairperson = widget.chairperson.replaceAll("\\n", "\n");
    } else {
      chairperson = "";
    }
    if (!(widget.moderator == null)) {
      moderator = widget.moderator.replaceAll("\\n", "\n");
    } else {
      moderator = "";
    }

    // _reminderDiag = Dialog(
    //   backgroundColor: Colors.indigo,
    //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
    //   child: Container(
    //     height: 225.0,
    //     width: 150.0,
    //     child: Column(
    //       mainAxisAlignment: MainAxisAlignment.center,
    //       children: <Widget>[
    //         Text(
    //           'Set Reminder',
    //           textAlign: TextAlign.center,
    //           style: TextStyle(fontSize: 20.0, color: Colors.white),
    //         ),
    //         FlatButton(
    //             onPressed: () {
    //               _scheduleNotification(0);
    //               Navigator.pop(context);
    //             },
    //             child: Text(
    //               'On time of program',
    //               style: TextStyle(color: Colors.white, fontSize: 18.0),
    //             )),
    //         FlatButton(
    //             onPressed: () {
    //               _scheduleNotification(5);
    //               Navigator.pop(context);
    //             },
    //             child: Text(
    //               '5 minutes before program',
    //               style: TextStyle(color: Colors.white, fontSize: 18.0),
    //             )),
    //         FlatButton(
    //             onPressed: () {
    //               _scheduleNotification(10);
    //               Navigator.pop(context);
    //             },
    //             child: Text(
    //               '10 minutes before program',
    //               style: TextStyle(color: Colors.white, fontSize: 18.0),
    //             )),
    //         FlatButton(
    //             onPressed: () {
    //               _scheduleNotification(15);
    //               Navigator.pop(context);
    //             },
    //             child: Text(
    //               '15 minutes before program',
    //               style: TextStyle(color: Colors.white, fontSize: 18.0),
    //             ))
    //       ],
    //     ),
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    double height = (MediaQuery.of(context).size.height);

    double fontSize = 15;
    double titleSize = 20;

    if (height > 900) {
      fontSize = 25;
      titleSize = 30;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Program Details'),
        centerTitle: true,
        actions: <Widget>[
          Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
              child: GestureDetector(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 8,
                    ),
                    Icon(
                      Icons.notification_important,
                      color: Colors.white,
                    ),
                    Text(
                      "Reminder",
                      style: TextStyle(fontSize: 10.0, color: Colors.white),
                    )
                  ],
                ),
                onTap: () {
                  _actionSheet(context);
                },
              )),
        ],
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            SizedBox(height: 20.0),
            Text(
              "Timings: " + widget.time,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: fontSize),
            ),
            SizedBox(height: 20.0),
            Container(
              color: Theme.of(context).primaryColor,
              margin: const EdgeInsets.only(top: 5.0),
              padding:
                  const EdgeInsets.only(left: 10.0, bottom: 10.0, top: 10.0),
              child: Text(
                widget.type,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: fontSize, color: Colors.white),
              ),
            ),
            Container(
              color: Theme.of(context).primaryColor,
              //margin: const EdgeInsets.only(top: 5.0),
              padding:
                  const EdgeInsets.only(left: 10.0, bottom: 10.0, top: 10.0),
              child: Text(
                "Topic",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: titleSize, color: Colors.white),
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              padding: const EdgeInsets.only(
                  left: 20.0, bottom: 10.0, top: 10.0, right: 20.0),
              child: Text(
                widget.topic.toString().replaceAll("\\n", "\n"),
                textAlign: TextAlign.justify,
                style: TextStyle(color: Colors.black, fontSize: fontSize),
              ),
            ),
            SizedBox(height: 20.0),
            Divider(
              height: 3.0,
              color: Colors.grey[400],
            ),
            chairperson == ""
                ? Container()
                : Container(
                    color: Theme.of(context).primaryColor,
                    padding: const EdgeInsets.only(
                        left: 20.0, bottom: 20.0, top: 20.0),
                    child: Text(
                      "Chairpersons",
                      textAlign: TextAlign.left,
                      style:
                          TextStyle(fontSize: titleSize, color: Colors.white),
                    ),
                  ),
            chairperson == ""
                ? Container()
                : Container(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 20.0),
                    child: Text(
                      chairperson,
                      textAlign: TextAlign.justify,
                      style: TextStyle(color: Colors.black, fontSize: fontSize),
                    ),
                  ),
                  moderator!=""?
            Container(
              color: Theme.of(context).primaryColor,
              padding:
                  const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20.0),
              child: Text(
                "Moderator",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: titleSize, color: Colors.white),
              ),
            ):Container(),
            moderator!=""?
            Container(
              padding: const EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
              child: Text(
                moderator,
                textAlign: TextAlign.justify,
                style: TextStyle(color: Colors.black, fontSize: fontSize),
              ),
            ):Container(),
            speaker!=""?
            Container(
              color: Theme.of(context).primaryColor,
              padding:
                  const EdgeInsets.only(left: 20.0, bottom: 20.0, top: 20.0),
              child: Text(
                "Speaker / Faculty",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: titleSize, color: Colors.white),
              ),
            ):Container(),
            speaker!=""?
            Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Text(
                speaker,
                textAlign: TextAlign.justify,
                style: TextStyle(color: Colors.black, fontSize: fontSize),
              ),
            ):Container(),
          ],
        ),
      ),
    );
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              // Navigator.of(context, rootNavigator: true).pop();
              // await Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => SecondScreen(payload),
              //   ),
              // );
            },
          )
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }

    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Program()),
    );
  }

  Future _scheduleNotification(int timer) async {
    //String time = '14:33:00.000';
    //date = yyyy-mm-dd
    print(widget.alertdate.split('').reversed.join());
    int id = int.parse(widget.id);
    String formattedString =
        '${widget.alertdate.split('').reversed.join()} ${widget.alerttime}';
    int scheduleid = timer + id;
    var scheduledNotificationDateTime =
        DateTime.parse(formattedString).subtract(Duration(minutes: timer));
    var vibrationPattern = Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your other channel id',
        'your other channel name',
        'your other channel description',
        icon: '@mipmap/ic_launcher',
        largeIcon: '@mipmap/ic_launcher',
        largeIconBitmapSource: BitmapSource.Drawable,
        vibrationPattern: vibrationPattern,
        color: Theme.of(context).primaryColor
        //const Color.fromARGB(255, 32, 178, 170)
        );

    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        scheduleid,
        '${widget.topic}',
        timer != 0 ? 'will be starting in $timer minutes' : 'has started',
        scheduledNotificationDateTime,
        platformChannelSpecifics);
  }

  void containerForSheet<T>({BuildContext context, Widget child}) {
    showCupertinoModalPopup<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {
      value != null ? showSimpleFlushbar(context, value) : Container();
    });
  }

  void showSimpleFlushbar(BuildContext context, var value) {
    Flushbar(
      icon: Icon(
        Icons.notifications,
        color: Colors.indigo,
      ),
      message: 'You will be reminded $value',
      duration: Duration(seconds: 5),
    )..show(context);
  }

  void _actionSheet(BuildContext context) {
    return containerForSheet<String>(
      context: context,
      child: CupertinoActionSheet(
          title: const Text(
            'Reminder',
            style: TextStyle(fontSize: 20),
          ),
          message: const Text(
            'Set reminder for this topic',
            style: TextStyle(fontSize: 19),
          ),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: const Text(
                'on time of the program',
                style: TextStyle(fontSize: 18),
              ),
              onPressed: () {
                _scheduleNotification(0);
                Navigator.pop(context, 'on time of the program');
              },
            ),
            CupertinoActionSheetAction(
              child: const Text(
                '5 minutes before the program',
                style: TextStyle(fontSize: 18),
              ),
              onPressed: () {
                _scheduleNotification(5);
                Navigator.pop(context, '5 minutes before the program starts');
              },
            ),
            CupertinoActionSheetAction(
              child: const Text(
                "10 minutes before the program",
                style: TextStyle(fontSize: 18),
              ),
              onPressed: () {
                _scheduleNotification(10);
                Navigator.pop(context, "10 minutes before the program starts");
              },
            ),
            CupertinoActionSheetAction(
              child: const Text(
                "15 minutes before the program",
                style: TextStyle(fontSize: 18),
              ),
              onPressed: () {
                _scheduleNotification(15);
                Navigator.pop(context, "15 minutes before the program starts");
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            child: const Text('Cancel'),
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
          )),
    );
  }
}
