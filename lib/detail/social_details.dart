import 'package:flutter/material.dart';

class SocialDetails extends StatefulWidget {
   String time,topic,type,details,img;
  SocialDetails(
      {Key key,  
      this.topic, 
      this.type,
      this.details,
      this.time,
      this.img
      })
      : super(key: key);
  @override
  _SocialDetailsState createState() => _SocialDetailsState();
}

class _SocialDetailsState extends State<SocialDetails> {
  @override
  Widget build(BuildContext context) {
        double height = (MediaQuery.of(context).size.height);

    double fontSize = 15;
    //double titleSize = 20;

    if (height > 900) {
      fontSize = 25;
     // titleSize = 30;
    }
    return Container(
      child: ListView(
        children:<Widget>[
          SizedBox(height: 20.0),
            Text(
              "Timings: " + widget.time,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: fontSize),
            ),
            SizedBox(height: 20.0),
             Container(
              color: Theme.of(context).primaryColor,
              margin: const EdgeInsets.only(top: 5.0),
              padding:
                  const EdgeInsets.only(left: 10.0, bottom: 10.0, top: 10.0),
              child: Text(
                widget.type,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: fontSize, color: Colors.white),
              ),
            ),
            Image(
      image: AssetImage(widget.img), fit: BoxFit.fill,
      height:  height>950?400:200,
    ),
             SizedBox(height: 20.0),
             Container(
              color: Theme.of(context).primaryColor,
              margin: const EdgeInsets.only(top: 5.0),
              padding:
                  const EdgeInsets.only(left: 10.0, bottom: 10.0, top: 10.0),
              child: Text(
                widget.topic,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: fontSize, color: Colors.white),
              ),
            ),
            SizedBox(height: 20.0),
            widget.details!=""?
            Text(
              widget.details,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black, fontSize: fontSize),
            ):Container(),
        ],
      ),
    );
  }
}