import 'package:flutter/material.dart';

class ExploreDetails extends StatefulWidget{
String img,info,title;

ExploreDetails({Key key, this.img, this.info, this.title}): super(key: key);
  @override
  _ExploreDetails createState()=>_ExploreDetails();
    }
  
  class _ExploreDetails extends State<ExploreDetails> {
  @override
  Widget build(BuildContext context) {
    double width = (MediaQuery.of(context).size.width);
    double height = (MediaQuery.of(context).size.height);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorLight,
      appBar: AppBar(title: Text(widget.title),centerTitle: true,),
      body: Container(
        color: Theme.of(context).primaryColorLight,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              child: Hero(
                tag: widget.title,
                child: new Image(
                    image: new NetworkImage(widget.img),fit: BoxFit.fill,
                    height: height>950?400:200,
                    width: width,

                  ),
              ),
            ),
            Container(padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),child: Text(widget.info,textAlign: TextAlign.justify,style: TextStyle(fontSize:15,color: Colors.black ),),),
        
          ],
        ),
      ),
    );
  }
}