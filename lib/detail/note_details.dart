import 'dart:async';
import 'dart:typed_data';
import 'package:intl/intl.dart';
import 'package:apsicon2019/dataholder/DBHelper_notes.dart';
import 'package:apsicon2019/model/notes_data.dart';
import 'package:apsicon2019/pages/notes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotesDetails extends StatefulWidget {
  String title, content, type;
  int id;

  NotesDetails({Key key, this.id, this.title, this.content, this.type})
      : super(key: key);

  @override
  _NotesDetails createState() => _NotesDetails();
}

class _NotesDetails extends State<NotesDetails> {
  final TextEditingController _textController = new TextEditingController();
  final TextEditingController _textController2 = new TextEditingController();
  var flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  @override
  void initState() {
    
    super.initState();
    _textController.text = widget.title;
    _textController2.text = widget.content;

    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              // Navigator.of(context, rootNavigator: true).pop();
              // await Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => SecondScreen(payload),
              //   ),
              // );
            },
          )
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }

    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Notes()),
    );
  }

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();
  DateTime tv = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: selectedDate,
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        //selectedDate = picked;
        widget.title = _textController.text;
        tv = picked;
        int day = tv.day - DateTime.now().day;
        //Time fn here
        _selectTime(context, day);
      });
  }

  Future<Null> _selectTime(BuildContext context, int day) async {
    final TimeOfDay picked2 = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked2 != null && picked2 != selectedTime)
      setState(() {
        selectedTime = picked2;
        int min = (selectedTime.hour * 60) + selectedTime.minute;
        _scheduleNotification(day, min);
      });
  }


// showNotification() async {
//     var android = new AndroidNotificationDetails(
//         'channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
//         priority: Priority.High,importance: Importance.Max
//     );
//     var iOS = new IOSNotificationDetails();
//     var platform = new NotificationDetails(android, iOS);
//     await flutterLocalNotificationsPlugin.show(
//         0, 'New Video is out', 'Flutter Local Notification', platform,
//         payload: 'Nitish Kumar Singh is part time Youtuber');
//   }


  Future _scheduleNotification(int day, int min) async {
    if (day == 0) {
      DateTime now = DateTime.now();
      //String formattedDate = DateFormat('kk:mm:ss \n EEE d MMM').format(now);
      int now2 = (now.hour * 60) + now.minute+ now.second;
      min = min - now2;
    }
//String formattedString =;
    var scheduledNotificationDateTime =
        DateTime.now().add(Duration(days: day, minutes: min));
        // var scheduledNotificationDateTime2 =
        // DateTime.parse(formattedString);
    var vibrationPattern = Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your other channel id',
        'your other channel name',
        'your other channel description',
        icon: '@mipmap/ic_launcher',
        //sound: 'slow_spring_board',
        largeIcon: '@mipmap/ic_launcher',
        largeIconBitmapSource: BitmapSource.Drawable,
        vibrationPattern: vibrationPattern,
        color: Theme.of(context).primaryColor 
        //const Color.fromARGB(255, 255, 0, 0)
        );

    var iOSPlatformChannelSpecifics =
        IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        day+min,
        'Reminder',
        '${widget.title}',
        scheduledNotificationDateTime,
        platformChannelSpecifics);
  }

  @override
  Widget build(BuildContext context) {
    var dbHelper = new DBHelperNotes();

    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        actions: <Widget>[
          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
            child: _textController.text != ""
                ? GestureDetector(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 8,
                        ),
                        Icon(
                          Icons.save,
                          color: Colors.white,
                        ),
                        Text(
                          "Save",
                          style: TextStyle(fontSize: 10.0, color: Colors.white),
                        )
                      ],
                    ),
                    onTap: () {
                      if (widget.type == "Edit") {
                        var schedule = NotesData();
                        schedule.id = widget.id;
                        schedule.note = _textController.text;
                        schedule.content = _textController2.text;

                        dbHelper.updateToNotes(schedule);
                      } else {
                        var schedule = NotesData();
                        schedule.note = _textController.text;
                        schedule.content = _textController2.text;
                        dbHelper.addToNotes(schedule);
                        setState(() {
                          widget.type = "Edit";
                        });
                      }
                    },
                  )
                : Container(),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 5.0, 0),
            child: widget.type == 'Edit'
                ? GestureDetector(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 8,
                        ),
                        Icon(
                          Icons.notification_important,
                          color: Colors.white,
                        ),
                        Text(
                          "Reminder",
                          style: TextStyle(fontSize: 10.0, color: Colors.white),
                        )
                      ],
                    ),
                    onTap: () {
                      _selectDate(context);
                    },
                  )
                : Container(),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 10.0,
          ),
          Container(
            padding: const EdgeInsets.only(left: 10.0),
            child: new TextField(
              onChanged: (_textController) {
                setState(() {
                  _textController = _textController;
                });
              },
              style: TextStyle(fontSize: 20.0, color: Colors.black),
              controller: _textController,
              decoration: new InputDecoration.collapsed(
                  hintText: "Title", hintStyle: TextStyle(fontSize: 20.0)),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Divider(
              height: 4.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
          Container(
            height: 400.0,
            child: ListView(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: new TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    controller: _textController2,
                    decoration:
                        new InputDecoration.collapsed(hintText: "Content"),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
