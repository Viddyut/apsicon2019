import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'login.dart';

class Register extends StatefulWidget {
  @override
  _Register createState() => new _Register();
}

class _Register extends State<Register> {
  String _email, _password, _mobile;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var _connectionStatus = 'Unknown';
  Connectivity connectivity;
  StreamSubscription<ConnectivityResult> subscription;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double width = (MediaQuery.of(context).size.width);
    double height = (MediaQuery.of(context).size.height);

    double header_fontSize, header_paddingTop, fontsize_widgets, button_height;

    if (height < 550) {
      header_paddingTop = 50.0;
      header_fontSize = 40.0;
      fontsize_widgets = 10.0;
      button_height = 30.0;
    } else {
      header_paddingTop = 100.0;
      header_fontSize = 40.0;
      fontsize_widgets = 15.0;
      button_height = 40.0;
    }

    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      connectivity = new Connectivity();
      subscription = connectivity.onConnectivityChanged
          .listen((ConnectivityResult result) {
        _connectionStatus = result.toString();
        print(_connectionStatus);
        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {
          setState(() {});
        }
      });
    }

    @override
    void dispose() {
      subscription.cancel();
      super.dispose();
    }

    User selectedUser;
    List<User> users = <User>[const User('Foo'), const User('Bar')];

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.fromLTRB(
                                15.0, header_paddingTop, 0.0, 0.0),
                            child: Text('SignUp.',
                                style: TextStyle(
                                    fontSize: header_fontSize,
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).primaryColor)))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 500.0,
                    child: Form(
                      key: _formKey,
                      child: ListView(
                        children: <Widget>[
                          TextFormField(
                            validator: (input) {
                              if (input.isEmpty) {
                                return 'Please enter email';
                              }
                            },
                            onSaved: (input) => _email = input,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.blueGrey[50],
                              hintText: 'EMAIL',
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: fontsize_widgets),
                              enabledBorder: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          TextFormField(
                            validator: (input) {
                              if (input.isEmpty) {
                                return 'Please enter password`';
                              }
                              if (input.length < 6) {
                                return 'Password needs to be at least 6 charecters';
                              }
                            },
                            onSaved: (input) => _password = input,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.blueGrey[50],
                              hintText: 'PASSWORD',
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: fontsize_widgets),
                              enabledBorder: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                            ),
                            obscureText: true,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          TextFormField(
                            validator: (input) {
                              if (input.isEmpty) {
                                return 'Please enter mobile number';
                              }
                            },
                            onSaved: (input) => _mobile = input,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.blueGrey[50],
                              hintText: 'MOBILE NUMBER',
                              focusedBorder: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: fontsize_widgets),
                              enabledBorder: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    const Radius.circular(5.0)),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          new DropdownButton<User>(
                            hint: new Text("Select a user"),
                            value: selectedUser,
                            onChanged: (User newValue) {
                              setState(() {
                                print(newValue.name);
                                selectedUser = newValue;
                              });
                            },
                            items: users.map((User user) {
                              return new DropdownMenuItem<User>(
                                value: user,
                                child: new Text(
                                  user.name,
                                  style: new TextStyle(color: Colors.black),
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                            height: 50.0,
                          ),
                          Container(
                            height: button_height,
                            child: Material(
                              borderRadius: BorderRadius.circular(200.0),
                              shadowColor: Theme.of(context).primaryColor,
                              color: Theme.of(context).primaryColor,
                              elevation: 7.0,
                              child: GestureDetector(
                                onTap: 
                                //() 
                                //{
                                  //                                subscription =
                                  //     connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
                                  //   _connectionStatus = result.toString();
                                  //   print(_connectionStatus);
                                  //   if (result == ConnectivityResult.wifi ||
                                  //       result == ConnectivityResult.mobile) {

                                  //     setState(() {signUp;});
                                  //   }
                                  // });
                                  // Navigator.of(context).push(
                                  //     new MaterialPageRoute(
                                  //         builder: (context) => new Login()));
                              //  },
                                signUp,
                                child: Center(
                                  child: Text(
                                    'Signup',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: fontsize_widgets,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Monserrat'),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 40.0,
                          ),
                          Container(
                            height: button_height,
                            color: Colors.transparent,
                            child: GestureDetector(
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.black,
                                        style: BorderStyle.solid,
                                        width: 1.0),
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: Center(
                                  child: Text(
                                    'Go Back',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: fontsize_widgets,
                                        fontFamily: 'Monserrat'),
                                  ),
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (context) => new Login()));
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> signUp() async {
    final formstate = _formKey.currentState;
    if (formstate.validate()) {
      formstate.save();
      try {
        AuthResult result = await FirebaseAuth.instance
            .createUserWithEmailAndPassword(email: _email, password: _password)
            .whenComplete(() {});
        FirebaseUser user = result.user;

        String _uid = user.uid;
        Map<String, String> data = <String, String>{
          //"Mobile_No": "$_mobile",
          "Email": "$_email",
          "User_id": "$_uid",
        };

        Firestore.instance.collection('accounts').add(data).whenComplete(() {
          print("successful");
        }).catchError(
          (e) => print(e),

// final snackbar = new SnackBar(
//        content: new Text("Submitted"),
//      ),
//      scaffoldKey.currentState.showSnackBar(snackbar)
        );
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => new Login()));
      } catch (e) {
        print(e);
      }
    }
  }
}

class User {
  const User(this.name);

  final String name;
}