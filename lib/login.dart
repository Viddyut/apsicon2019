import 'dart:async';
import 'package:apsicon2019/common/progress.dart';
import 'package:apsicon2019/home_second.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';

class Login extends StatefulWidget {
  @override
  _Login createState() => new _Login();
}

class _Login extends State<Login> {
  String _email, _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  ProgressBarHandler _handler;
  @override
  void initState() {
    super.initState();
    getUser().then((user) {
      if (user != null) {
        // send the user to the home page
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (context) => new HomeSecond(
                  user: user,
                )));
      }
    });
  }

  Future<FirebaseUser> getUser() async {
    return await _auth.currentUser();
  }

  Future<bool> _exitApp() async {
    await SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return true;
  }

  @override
  Widget build(BuildContext context) {
    double width = (MediaQuery.of(context).size.width);
    double height = (MediaQuery.of(context).size.height);
    print(width);
    print(height);

    double image_marginTop,
        image_marginHorizontal,
        sizedbox_height1,
        sizedbox_height2,
        sizedbox_height3,
        button_height,
        fontsize_widgets;

    if (height < 550) {
      image_marginTop = 70.0;
      image_marginHorizontal = 70.0;
      sizedbox_height1 = 5.0;
      sizedbox_height2 = 20.0;
      sizedbox_height3 = 30.0;
      button_height = 30.0;
      fontsize_widgets = 10.0;
    } else {
      image_marginTop = 120.0;
      image_marginHorizontal = 0.0;
      sizedbox_height1 = 20.0;
      sizedbox_height2 = 40.0;
      sizedbox_height3 = 60.0;
      button_height = 40.0;
      fontsize_widgets = 15.0;
    }
    var progressBar = ModalRoundedProgressBar(
    
      handleCallback: (handler) {
        _handler = handler;
      },
    );

    return WillPopScope(
      onWillPop: _exitApp,
      child: Stack(
        children: <Widget>[
          Scaffold(
            resizeToAvoidBottomInset: true,
            resizeToAvoidBottomPadding: true,
            body: Container(
              child: SingleChildScrollView(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Stack(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(image_marginHorizontal,
                                image_marginTop, image_marginHorizontal, 0.0),
                            child: Image(
                              image: new AssetImage("assets/apsicon.gif"),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(height: sizedbox_height1),
                    Container(
                      padding:
                          EdgeInsets.only(top: 35.0, left: 30.0, right: 30.0),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: TextFormField(
                                validator: (input) {
                                  if (input.isEmpty) {
                                    return 'Please enter email';
                                  }
                                },
                                onSaved: (input) => _email = input,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.email),
                                  filled: true,
                                  fillColor: Colors.blueGrey[50],
                                  hintText: 'EMAIL',
                                  focusedBorder: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                        const Radius.circular(5.0)),
                                  ),
                                  hintStyle: TextStyle(
                                      color: Colors.grey,
                                      fontSize: fontsize_widgets),
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                        const Radius.circular(5.0)),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: sizedbox_height2,
                            ),
                            Container(
                              child: TextFormField(
                                validator: (input) {
                                  if (input.isEmpty) {
                                    return 'Please enter password`';
                                  }
                                  if (input.length < 6) {
                                    return 'Password needs to be at least 6 charecters';
                                  }
                                },
                                onSaved: (input) => _password = input,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.vpn_key),
                                  filled: true,
                                  fillColor: Colors.blueGrey[50],
                                  hintText: 'PASSWORD',
                                  focusedBorder: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                        const Radius.circular(5.0)),
                                  ),
                                  hintStyle: TextStyle(
                                      color: Colors.grey,
                                      fontSize: fontsize_widgets),
                                  enabledBorder: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                        const Radius.circular(5.0)),
                                  ),
                                ),
                                obscureText: true,
                              ),
                            ),
                            SizedBox(
                              height: sizedbox_height3,
                            ),
                            Container(
                              height: button_height,
                              child: GestureDetector(
                                  child: Material(
                                    borderRadius: BorderRadius.circular(200.0),
                                    shadowColor: Theme.of(context).primaryColor,
                                    color: Theme.of(context).primaryColor,
                                    elevation: 7.0,
                                    child: Center(
                                      child: Text(
                                        'Login',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fontsize_widgets,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'Monserrat'),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    //  Navigator.of(context).push(new MaterialPageRoute(
                                    // builder: (context) => new HomeSecond()));
                                    _handler.show();
                                    signIn(context);
                                  }),
                            ),
                            
                            // SizedBox(
                            //   height: sizedbox_height2,
                            // ),
                            // Container(
                            //   height: button_height,
                            //   color: Colors.transparent,
                            //   child: GestureDetector(
                            //     child: Container(
                            //       decoration: BoxDecoration(
                            //           border: Border.all(
                            //               color: Colors.black,
                            //               style: BorderStyle.solid,
                            //               width: 1.0),
                            //           color: Colors.transparent,
                            //           borderRadius: BorderRadius.circular(20.0)),
                            //       child: Center(
                            //         child: Text(
                            //           'Register',
                            //           style: TextStyle(
                            //               fontWeight: FontWeight.bold,
                            //               fontSize: fontsize_widgets,
                            //               fontFamily: 'Monserrat'),
                            //         ),
                            //       ),
                            //     ),
                            //     onTap: () {
                            //       Navigator.of(context).push(new MaterialPageRoute(
                            //           builder: (context) => new Register()));
                            //     },
                            //   ),
                            // )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          progressBar,
        ],
      ),
    );
  }

  Future<void> signIn(BuildContext context) async {
    final formstate = _formKey.currentState;
    
    if (formstate.validate()) {
      formstate.save();
      try {
        AuthResult result = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: _email.trim(), password: _password)
            .catchError((onError) {
          _handler.dismiss();
          print("Error is :" + onError.toString());
          showSimpleFlushbar(context);
        });

        FirebaseUser user = result.user;

        Navigator.of(context).push(new MaterialPageRoute(
            builder: (context) => new HomeSecond(
                  user: user,
                )));
      } catch (e) {
        print(e);
      }
    }
    _handler.dismiss();
  }

  void showSimpleFlushbar(BuildContext context) {
    Flushbar(
      icon: Icon(
        Icons.info,
        color: Colors.red,
      ),
      message: 'Please enter the valid credentials',

      // mainButton: FlatButton(
      //   child: Text(
      //     'Click Me',
      //     style: TextStyle(color: Theme.of(context).accentColor),
      //   ),
      //   onPressed: () {},
      // ),

      duration: Duration(seconds: 5),
    )..show(context);
  }
}
