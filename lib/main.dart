import 'package:apsicon2019/coverpage.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final primary = const Color(0xFF3BB6C5);
  final secondary = const Color(0xFFC2E7EF);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'APSICON 2019',
      theme: ThemeData(
        primaryColor:primary,
        primaryColorLight:secondary 
      ),
      //#e1e1f9
      home: 
      //title: 'APSICON 2019'
     //MyHomePage()
     //Login()
     //Splash(),
     Cover()
    );
  }
}
