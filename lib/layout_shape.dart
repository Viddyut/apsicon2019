import 'dart:core';
import 'dart:math';

import 'package:flutter/material.dart';

class LayoutShape extends CustomClipper<Path> {
  
  @override
  Path getClip(Size size) {

    double width = size.width;
    double height = size.height;
    double rheight = height - height / 3;
    double oneThird = width / 4;

    final path = Path()
    
      ..lineTo(0, height)
      ..lineTo(oneThird, height)
      ..arcToPoint(Offset(width-(width/4), height), radius: Radius.circular(20.0))
      ..lineTo(width-(width/4), height)
      ..lineTo(width, height)
      ..lineTo(width, 0)
      ..lineTo(0, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
