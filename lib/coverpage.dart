import 'dart:async';
import 'dart:ui' as prefix0;
import 'package:flare_dart/actor.dart';
import 'package:apsicon2019/home_second.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

import 'login.dart';

class Cover extends StatefulWidget{

  @override
  _Cover createState()=>_Cover();
  }
  
  class _Cover extends State<Cover> {
final FirebaseAuth _auth = FirebaseAuth.instance;
    @override
  void initState() {
  
    super.initState();

    getUser().then((user) {
      if (user != null) {
        // send the user to the home page
      Timer(Duration(seconds: 5), ()=> Navigator.of(context).push(
            new MaterialPageRoute(builder: (context) => new HomeSecond(user: user,))));
      }
      else{
Timer(Duration(seconds: 5), ()=> Navigator.of(context).push(new MaterialPageRoute(
                                    builder: (context) => new Login())));
      }
    });
    
  }

  Future<FirebaseUser> getUser() async {
    return await _auth.currentUser(); 
  }
  
  @override
  Widget build(BuildContext context) {
    double width = (MediaQuery.of(context).size.width);
    double height = (MediaQuery.of(context).size.height);
    return Scaffold(
      body: BackdropFilter(
        filter: prefix0.ImageFilter.blur(sigmaX: 5,sigmaY: 5),
        child: Container(
          height: height,
          width: width,
          child: 
          //FlareActor('assets/anime.flr',animation: "anime",fit: BoxFit.contain,)
          Image.asset('assets/cover.jpg',fit: BoxFit.fill,),
        ),
      ),
    );
  }
}