import 'package:apsicon2019/pages/contact_us.dart';
import 'package:apsicon2019/pages/notes.dart';
import 'package:apsicon2019/pages/notifications.dart';
import 'package:apsicon2019/pages/venue.dart';
import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(context),
          _createDrawerItem(icon: Icons.notifications,text: 'Notifications',onTap: (){ Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new Notifications())); },context: context),
          _createDrawerItem(icon: Icons.note, text: 'Notes',onTap: (){ Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new Notes())); },context:context),
          _createDrawerItem(icon: Icons.inbox, text: 'Coupons',onTap: (){ Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new Notes())); },context:context),
          _createDrawerItem(icon: Icons.location_on, text: 'Venue',onTap: (){ Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new Venue())); },context:context),
          Divider(),
          _createDrawerItem(icon: Icons.contact_phone, text:'Contact Us',onTap: (){ Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new ContactUs())); },context:context),
          _createDrawerItem(icon: Icons.exit_to_app, text:'Log out',onTap: (){ Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new ContactUs())); },context:context),
         
          // ListTile(
          //   title: Text('0.0.1'),
          //   onTap: () {},
          // ),
        ],
      ),
    );
  }
  Widget _createHeader(BuildContext context) {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor
          // image: DecorationImage(
          //     fit: BoxFit.fill,
          //     image:  AssetImage('path/to/header_background.png'))
              ),
      child: Stack(children: <Widget>[
        Positioned(
            bottom: 12.0,
            left: 16.0,
            child: Text("APSICON 2019",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500))),
      ]));
}
Widget _createDrawerItem(
    {IconData icon, String text, GestureTapCallback onTap,BuildContext context}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon,color: Theme.of(context).primaryColor,),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text),
        )
      ],
    ),
    onTap: onTap,
  );
}
}