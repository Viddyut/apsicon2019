import 'dart:core';

import 'package:flutter/material.dart';

class LayoutTop extends CustomClipper<Path> {
  
  @override
  Path getClip(Size size) {

    double width = size.width;
    double height = size.height;
  

    final path = Path()
    
      ..lineTo(0, height)
      ..arcToPoint(Offset(width, height), radius: Radius.circular(70.0))
      ..lineTo(width, 0)
      ..lineTo(0, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}
