import 'package:apsicon2019/common/progress.dart';
import 'package:apsicon2019/pages/my_programs.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserProfile extends StatefulWidget {
  final FirebaseUser user;
  UserProfile({Key key, this.user}) : super(key: key);
  @override
  _UserProfile createState() => _UserProfile();
}

class _UserProfile extends State<UserProfile> {
  String name,
      email,
      contact,
      accomodation,
      accomodation_type,
      accompanaying_person,
      amount_paid,
      check_in,
      check_out,
      hotel,
      registration_type;

  var doc;
  ProgressBarHandler _handler;

  @override
  void initState() {
    super.initState();
    getData();
  }

  getData() {
    doc = Firestore.instance
        .collection('user_data')
        .where('email', isEqualTo: widget.user.email)
        .limit(1);
    doc.getDocuments().then((data) {
      if (data.documents.length > 0) {
        setState(() {
          name = data.documents[0].data['name'];
          email = data.documents[0].data['email'];
          contact = data.documents[0].data['contact'];
          accomodation = data.documents[0].data['accomodation'];
          accomodation_type = data.documents[0].data['accomodation_type'];
          accompanaying_person = data.documents[0].data['accompanaying_person'];
          amount_paid = data.documents[0].data['amount_paid'];
          check_in = data.documents[0].data['check_in'];
          check_out = data.documents[0].data['check_out'];
          hotel = data.documents[0].data['hotel'];
          registration_type = data.documents[0].data['registration_type'];
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Firestore.instance.enablePersistence(true);
    var progressBar = ModalRoundedProgressBar(
      //getting the handler
      handleCallback: (handler) {
        _handler = handler;
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("My Profile"),
        centerTitle: true,
        actions: <Widget>[ Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
              child: GestureDetector(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 8,
                    ),
                    Icon(
                      Icons.note,
                      color: Colors.white,
                    ),
                    Text(
                      "My Programs",
                      style: TextStyle(fontSize: 10.0, color: Colors.white),
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context) => new MyPrograms(name: name,)));
                },
              )),],
      ),
      body: Container(
          child: Stack(
        children: <Widget>[
          ListView(
            padding: const EdgeInsets.all(20.0),
            shrinkWrap: true,
            children: <Widget>[
              Text(
                name != null ? name : "",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black),
              ),
              SizedBox(
                width: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.email,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    email != null ? email : "",
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              SizedBox(
                width: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.call,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    contact != null ? contact : "",
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Registration Type: ",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 12),
              ),
              Text(registration_type != null ? registration_type : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 5.0,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Accomodation: ",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 12),
              ),
              Text(accomodation != null ? accomodation : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 5.0,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Accompanying Person: ",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12)),
              Text(accompanaying_person != null ? accompanaying_person : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Amount Paid: ",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12)),
              Text(amount_paid != null ? amount_paid : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Accomodation Details ",
                  style: TextStyle(color: Theme.of(context).primaryColor)),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Name: ",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 12),
              ),
              Text(name != null ? name : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Hotel: ",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12)),
              Text(hotel != null ? hotel : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              
                  Text("Check in: ",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor, fontSize: 12)),
                  Text(check_in != null ? check_in : ""),
      

              SizedBox(height: 5.0),

                  Text("Check out: ",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor, fontSize: 12)),
                  Text(check_out != null ? check_out : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Accomodation Type: ",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12)),
              Text(accomodation_type != null ? accomodation_type : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text("Amount Paid: ",
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12)),
              Text(amount_paid != null ? amount_paid : ""),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                height: 2.0,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                height: 5.0,
              ),
            ],
          ),
          progressBar,
        ],
      )),
    );
  }
}
