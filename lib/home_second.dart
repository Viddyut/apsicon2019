import 'dart:async';
import 'dart:ui';
import 'package:apsicon2019/dataholder/DBHelper.dart';
import 'package:apsicon2019/layout_shape.dart';
import 'package:apsicon2019/layout_top.dart';
import 'package:apsicon2019/login.dart';
import 'package:apsicon2019/model/notification_data.dart';
import 'package:apsicon2019/pages/accomp_person_prog.dart';
import 'package:apsicon2019/pages/committee.dart';
import 'package:apsicon2019/pages/contact_us.dart';
import 'package:apsicon2019/pages/explore.dart';
import 'package:apsicon2019/pages/faculty.dart';
import 'package:apsicon2019/pages/floor_map.dart';
import 'package:apsicon2019/pages/notes.dart';
import 'package:apsicon2019/pages/notifications.dart';
import 'package:apsicon2019/pages/programs.dart';
import 'package:apsicon2019/pages/shuttleService.dart';
import 'package:apsicon2019/pages/social_events.dart';
import 'package:apsicon2019/pages/trade.dart';
import 'package:apsicon2019/pages/venue.dart';
import 'package:apsicon2019/user_profile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;
import 'package:onesignal/onesignal.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeSecond extends StatefulWidget {
  final FirebaseUser user;
  HomeSecond({Key key, this.user}) : super(key: key);

  @override
  _HomeSecond createState() => _HomeSecond();
}

class _HomeSecond extends State<HomeSecond> with SingleTickerProviderStateMixin {
  
  AnimationController _animationController;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  Dialog _logoutDiag;

  @override
  void initState() {
   
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    _animationController =
        AnimationController(duration: Duration(seconds: 10), vsync: this)
          ..repeat();

    _logoutDiag = Dialog(
      //backgroundColor: Colors.indigo,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      child: Container(
        height: 85.0,
        width: 150.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Are you sure?',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20.0, color: Colors.black),
            ),
            SizedBox(
              height: 2.0,
            ),
            Divider(
              height: 2.0,
              color: Colors.indigo,
              indent: 5.0,
              endIndent: 5.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                    onPressed: () {
                      _auth.signOut();
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (context) => new Login()));
                      //Navigator.pop(context);
                    },
                    child: Text(
                      'Log out',
                      style: TextStyle(color: Colors.black, fontSize: 18.0),
                    )),
                    
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Cancel',
                      style: TextStyle(color: Colors.black, fontSize: 18.0),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

_launchURL() async {
  const url = 'https://www.apsicon2019.in/travel';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

  @override
  Widget build(BuildContext context) {
    double width = (MediaQuery.of(context).size.width);
    double height = (MediaQuery.of(context).size.height);

    Firestore.instance.enablePersistence(true);

    OneSignal.shared.init('9e52757d-3e00-4817-9d86-ee9a82374d80', iOSSettings: {
      OSiOSSettings.autoPrompt: false,
      OSiOSSettings.inAppLaunchUrl: true
    });

    OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);

    OneSignal.shared.setNotificationReceivedHandler((notification) {
      String title = notification.payload.title; //notification title
      String subtitle = notification.payload.body; //notification message

      var schedule = NotificationData();
      schedule.title = title;
      schedule.body = subtitle;
      var dbHelper = new DBHelper();
      dbHelper.addToSchedule(schedule);
      print("Added to db");
    });

    OneSignal.shared.setNotificationOpenedHandler((handler) {
      String title = handler.notification.payload.title; //notification title
      String subtitle =
          handler.notification.payload.body; //notification message

      var schedule = NotificationData();
      schedule.title = title;
      schedule.body = subtitle;
      var dbHelper = new DBHelper();
      dbHelper.addToSchedule(schedule);
      print("Added to db");
    });

    NotchedShape shape;

    double bottombarHeight,
        bottomButtonsHeight,
        logoHeight,
        logoPosi,
        socialsprogramButtonSize,
        speakerButtonSize,
        orgButtonSize,
        upperbottomButtonsMargins,
        socialButtonTop,
        speakerButtonTop,
        socialButtonRight,
        speakerButtonLeft,
        logobg_height,
        wheelSize;
    bool fabSize;
    if (height < 600) {
      logobg_height =2.5;
      bottombarHeight = 40;
      bottomButtonsHeight = 60;
      logoHeight = 150;
      logoPosi = 80;
      socialsprogramButtonSize = 70;
      orgButtonSize = 60;
      speakerButtonSize = 50;
      upperbottomButtonsMargins = 10;
      speakerButtonTop = 80;
      socialButtonTop = 85;
      socialButtonRight = 80;
      speakerButtonLeft = 90;
      wheelSize = 40;
      fabSize = true;
      shape = null;
    } 
    
    else if(height>950){
      logobg_height =3;
      bottombarHeight = 70;
      bottomButtonsHeight = 140;
      logoHeight = 250;
      logoPosi = 115;
      socialsprogramButtonSize = 175;
      orgButtonSize = 165;
      speakerButtonSize = 140;
      upperbottomButtonsMargins = 0;
      speakerButtonTop = 190;
      socialButtonTop = 195;
      socialButtonRight = 90;
      speakerButtonLeft = 110;
      wheelSize = 75;
      fabSize = false;
      shape = CircularNotchedRectangle();
    }
    
    else {
      logobg_height =2.5;
      bottombarHeight = 70;
      bottomButtonsHeight = 90;
      logoHeight = 250;
      logoPosi = 135;
      socialsprogramButtonSize = 110;
      orgButtonSize = 100;
      speakerButtonSize = 80;
      upperbottomButtonsMargins = 0;
      speakerButtonTop = 100;
      socialButtonTop = 105;
      socialButtonRight = 90;
      speakerButtonLeft = 110;
      wheelSize = 75;
      fabSize = false;
      shape = CircularNotchedRectangle();
    }
    Future<bool> _exitApp() async {
      await SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      return true;
    }

    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: Text(""),
        centerTitle: true,
        actions: <Widget>[
                  Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
              child: GestureDetector(
                child: Column(
                  
                  children: <Widget>[
                    SizedBox(
                      height: 8,
                    ),
                    Icon(
                      Icons.local_hotel,
                      color: Colors.orange[800],
                    ),
                    Text(
                      "Travel Details",
                      style: TextStyle(fontSize: 8.0, color: Colors.orange[800]),
                    )
                  ],
                ),
                onTap: () {
                
                  _launchURL();
                  
                },
              )),
         
          Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 5.0, 0),
              child: GestureDetector(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 8,
                    ),
                    Icon(
                      Icons.person_add,
                      color: Colors.orange[800],
                    ),
                    Text(
                      "Accompanying\nPerson Program",
                      style: TextStyle(fontSize: 8.0, color: Colors.orange[800]),textAlign: TextAlign.center,
                    )
                  ],
                ),
                onTap: () {
                  Navigator.of(context).push(
                      new MaterialPageRoute(builder: (context) => new AccompProgram()));
                },
              )),
          Container(
              margin: const EdgeInsets.fromLTRB(0, 0, 5.0, 0),
              child: GestureDetector(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 8,
                    ),
                    Icon(
                      Icons.subdirectory_arrow_left,
                      color: Colors.orange[800],
                    ),
                    Text(
                      "Log out",
                      style: TextStyle(fontSize: 8.0, color: Colors.orange[800]),
                    )
                  ],
                ),
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => _logoutDiag);
                },
              )),
        ],
      ),
      
      body: WillPopScope(
        onWillPop: _exitApp,
        child: Container(
          child: Column(
            children: <Widget>[
              
              Flexible(
                flex: 8,
                child: ClipPath(
                  clipper: LayoutShape(),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColorLight),
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Positioned(
                              left: width / 4,
                              //speakerButtonLeft,
                              top: (height / 2) - speakerButtonTop,
                              child: GestureDetector(
                                child: imgs(context, 'assets/speakers.png',
                                    speakerButtonSize),
                                onTap: () {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) => new Faculty()));
                                },
                              ),
                            ),
                            Positioned(
                              top: (height / 2) - socialButtonTop,
                              //right:width/4,
                              //socialButtonRight,
                              left: (width / 4) + (width / 4),
                              child: GestureDetector(
                                child: imgs(context, 'assets/social.png',
                                    socialsprogramButtonSize),
                                onTap: () {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new SocialEvents()));
                                },
                              ),
                            ),
                            Positioned(
                              top: (height / 2.05),
                              left: upperbottomButtonsMargins,
                              child: GestureDetector(
                                child: imgs(context, 'assets/programs.png',
                                    socialsprogramButtonSize),
                                onTap: () {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) => new Program()));
                                },
                              ),
                            ),
                            Positioned(
                              top: (height / 2.05),
                              // - 10,
                              right: upperbottomButtonsMargins,
                              child: GestureDetector(
                                child: imgs(
                                    context, 'assets/org.png', orgButtonSize),
                                onTap: () {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new Commitee()));
                                },
                              ),
                            ),

                            Positioned(
                              top: 0,
                              left: 0,
                              child: ClipPath(
                                clipper: LayoutTop(),
                                child: Container(
                                  width: width,
                                  height: height - (height / logobg_height),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('assets/lord.jpg'),
                                        fit: BoxFit.fill),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        top: 0,
                                        bottom: 0,
                                        left: 0,
                                        right: 0,
                                        child: BackdropFilter(
                                          filter: ImageFilter.blur(
                                              sigmaX: 2.5, sigmaY: 2.5),
                                          child: Container(
                                            color: Colors.black.withOpacity(0),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        top: 0,
                                        left: (width / 2) - logoPosi,
                                        child: Container(
                                            height: logoHeight,
                                            alignment: Alignment.center,
                                            child:
                                                Image.asset('assets/logo.png')),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              
              Flexible(
                flex: 2,

                child: Container(
                  decoration:
                      new BoxDecoration(color: Theme.of(context).primaryColor),
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        left: 5.0,
                        top: 25,
                        child: GestureDetector(
                          child: imgs(context, 'assets/contacts.png',
                              bottomButtonsHeight),
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (context) => new ContactUs()));
                          },
                        ),
                      ),
                      Positioned(
                        left: width/4,
                        top: 25,
                        child: GestureDetector(
                          child: imgs(context, 'assets/shuttle.png',
                              bottomButtonsHeight),
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (context) => new ShuttleServices()));
                          },
                        ),
                      ),
                      Positioned(
                        left: width/4 + width/4+10,
                        top: 25,
                        child: GestureDetector(
                          child: imgs(context, 'assets/notes.png',
                              bottomButtonsHeight),
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (context) => new Notes()));
                          },
                        ),
                      ),
                      Positioned(
                        right: 5.0,
                        top: 25,
                        child: GestureDetector(
                          child: imgs(
                              context, 'assets/noti.png', bottomButtonsHeight),
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                                builder: (context) => new Notifications()));
                          },
                        ),
                      )
                    ],
                  ),
                ),
                //),
              )
            ],
          ),
        ),
      ),

      backgroundColor: Theme.of(context).primaryColor,

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      floatingActionButton: FloatingActionButton(
        mini: fabSize,
        child: AnimatedBuilder(
          animation: _animationController,
          child: Container(
            height: wheelSize,
            width: wheelSize,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/wheel.png'), fit: BoxFit.fill)),
          ),
          builder: (context, child) => 
          Transform.rotate(
            child: child,
            angle: _animationController.value * 5.0 * math.pi,
          ),
          
        ),
        onPressed: () {},
        backgroundColor: Colors.white,
      ),
      
      bottomNavigationBar: Container(
        height: bottombarHeight,
        child: BottomAppBar(
          color: Colors.indigo,
          shape: shape,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                  height: bottombarHeight,
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: new Image.asset('assets/trade.png'),
                    onTap: () {
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (context) => new Sponsors()));
                    },
                  )),
              Container(
                  height: bottombarHeight,
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: new Image.asset('assets/venue.png'),
                    onTap: () {
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (context) => new Venue()));
                    },
                  )),
              Container(
                  height: bottombarHeight,
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: new Image.asset('assets/explore.png'),
                    onTap: () {
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (context) => new Explore()));
                    },
                  )),
              Container(
                  height: bottombarHeight,
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: new Image.asset('assets/profile.png'),
                    onTap: () {
                      Navigator.of(context).push(new MaterialPageRoute(
                          builder: (context) => new UserProfile(
                                user: widget.user,
                              )));
                    },
                  )),
            ],
          ),
        ),
      ),);
  }

  Widget card(BuildContext context, String img, MaterialType type) {
    final color = const Color(0xFFDE6C13);
    return Material(
      //DE6C13 orange
      //C2E7EF light green
      elevation: 5.0,
      color: color,
      type: type,
      shape: type == MaterialType.card
          ? BeveledRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(15.0),
                  bottomRight: Radius.circular(15.0)),
            )
          : null,
      // shadowColor: Theme.of(context).primaryColor,
      child: Column(
        children: <Widget>[
          SizedBox(height: 15.0),
          Container(
            height: 75,
            alignment: Alignment.center,
            child: Image.asset(
              img,
              fit: BoxFit.fill,
            ),
          ),
          Text("data"),
        ],
      ),
    );
  }

  Widget imgs(BuildContext context, String img, double height) {
    return Container(height: height, alignment: Alignment.center, child: Image.asset(img));
  }
}
