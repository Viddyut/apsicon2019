import 'package:apsicon2019/model/notification_data.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'dart:async';
import 'package:path_provider/path_provider.dart';

class DBHelper {
  final String TABLE_NAME = "Notifications";

  static Database db_instance;

  Future<Database> get db async {
    if (db_instance == null) db_instance = await initDB();
    return db_instance;
  }

//Creates database
  Future<Database> initDB() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "apsicon.db");
    var db = await openDatabase(path, version: 1, onCreate: onCreateFunc);
    return db;
  }

//Creates table
  void onCreateFunc(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $TABLE_NAME(id INTEGER PRIMARY KEY AUTOINCREMENT,title TEXT,body TEXT);');
  }

//Retrieves data from table
  Future<List<NotificationData>> getSchedule() async {
    var db_connection = await db;
    List<Map> list = await db_connection.rawQuery('SELECT * FROM $TABLE_NAME');
    List<NotificationData> schedules = new List();
    for (int i = 0; i < list.length; i++) {
      NotificationData schedule = new NotificationData();
      schedule.id = list[i]['id'];
      schedule.title = list[i]['title'];
      schedule.body = list[i]['body'];

      schedules.add(schedule);
    }
    return schedules;
  }

//Inserts data into table
  void addToSchedule(NotificationData schedule) async {
    var db_connection = await db;
    String query =
        'INSERT INTO $TABLE_NAME(title,body) VALUES(\'${schedule.title}\',\'${schedule.body}\')';
    await db_connection.transaction((transcation) async {
      return await transcation.rawInsert(query);
    });
  }

//Deletes data from table
  void deleteFromSchedule(NotificationData schedule) async {
    var db_connection = await db;
    String query = 'DELETE FROM $TABLE_NAME WHERE id=${schedule.id}';

    await db_connection.transaction((transcation) async {
      return await transcation.rawDelete(query);
    });
  }
}
