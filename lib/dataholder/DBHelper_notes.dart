import 'package:apsicon2019/model/notes_data.dart';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'dart:async';
import 'package:path_provider/path_provider.dart';

class DBHelperNotes {
  final String TABLE_NAME = "Notes";

  static Database db_instance;

  Future<Database> get db async {
    if (db_instance == null) db_instance = await initDB();
    return db_instance;
  }

//Creates database
  Future<Database> initDB() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "apsicon3.db");
    var db = await openDatabase(path, version: 1, onCreate: onCreateFunc);
    return db;
  }

//Creates table
  void onCreateFunc(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $TABLE_NAME(id INTEGER PRIMARY KEY AUTOINCREMENT,note TEXT,content TEXT);');
  }

//Retrieves data from table
  Future<List<NotesData>> getNotes() async {
    var db_connection = await db;
    List<Map> list = await db_connection.rawQuery('SELECT * FROM $TABLE_NAME');
    List<NotesData> schedules = new List();
    for (int i = 0; i < list.length; i++) {
      NotesData schedule = new NotesData();
      schedule.id = list[i]['id'];
      schedule.note = list[i]['note'];
      schedule.content = list[i]['content'];

      schedules.add(schedule);
    }
    return schedules;
  }

//Inserts data into table
  void addToNotes(NotesData schedule) async {
    var db_connection = await db;
    String query = 'INSERT INTO $TABLE_NAME(note,content) VALUES(\'${schedule.note}\',\'${schedule.content}\')';
    await db_connection.transaction((transcation) async {
      return await transcation.rawInsert(query);
    });
  }

  //Update data into table
  void updateToNotes(NotesData schedule) async {
    var db_connection = await db;
    String query = 'UPDATE $TABLE_NAME SET note = \'${schedule.note}\', content = \'${schedule.content}\' WHERE id=\'${schedule.id}\'';
    await db_connection.transaction((transcation) async {
      return await transcation.rawUpdate(query);
    });
  }

//Deletes data from table
  void deleteFromNotes(NotesData schedule) async {
    var db_connection = await db;
    String query = 'DELETE FROM $TABLE_NAME WHERE id=${schedule.id}';

    await db_connection.transaction((transcation) async {
      return await transcation.rawDelete(query);
    });
  }
}
